*CMZ :  4.01/00 11/02/2023  16.38.29  by  Michael Scheer
*CMZ :  4.00/17 29/11/2022  13.54.34  by  Michael Scheer
*CMZ :  4.00/16 17/09/2022  15.45.00  by  Michael Scheer
*CMZ :  4.00/15 02/06/2022  10.21.32  by  Michael Scheer
*CMZ :  4.00/11 28/06/2021  10.33.06  by  Michael Scheer
*-- Author : Michael Scheer
      subroutine amprep_nobsv_omp

*KEEP,trackf90u.
      include 'trackf90u.cmn'
*KEEP,spectf90u.
      include 'spectf90u.cmn'
*KEND.
      use sourcef90
      use observf90
      use afreqf90
      use bunchmod
      use omp_lib

      implicit none

      double precision, dimension (:), allocatable :: wsspec,frq,ecmx,spow
      double precision, dimension (:,:), allocatable :: wsstokes

      real, dimension (:), allocatable :: pherr,pherrc
      real, dimension(:), allocatable :: phiran

      real xran(5),pran(2),rr(2)

      double complex apol,amp0(3),damp(3),amp(3),zexp,
     &  apolh,apolr,apoll,apol45,phbu,refl(3),wpola(3),stokesv(4,3)

      double precision :: dtelec0,dtelec,dtpho,t0,perlen,dph,dobs(3),drn(3),cosang,ang,
     &  dobsn(3),r0(3),r(3),dr(3),t,dt,dist,obs(3),dlam,om,om1,fd,dist0,dl,
     &  stok1,stok2,stok3,stok4,speck,sqnbunch,sqnphsp,pow,pkerr,obs0(3),
     &  x2,y2,z2,vx2,vy2,vz2,eix,eiy,eiz,efx,efy,efz,vn,vf0,v,obscen(3),
     &  xi,yi,zi,vxi,vyi,vzi,xe,ye,ze,vxe,vye,vze,bshift=0.5d0,gamma,
     &  alphah,alphav,beta0h,beta0v,dpp,zpp,zz,zpi,ypi,zzp,zp,yp,yyp,yy,
     &  sigh,sigph,sigv,sigpv,s0v,s0h,gammav,gammah,fillb(29),zpmax,
     &  deflpar,xkx,pathlen,tof,dtdpp,t1,t2,distcorr,b0par(3),xb0par(3),
     &  b0opt,xb0opt,ab0(3),b0p(3),ds,upow,udgamtot,flow,fhigh,dum(3),dist00,
     &  obangh0,obangh,obangv0,obangv,drn00(3),drn0(3),dr0(3),r00(3),dr00(3),
     &  rm(3),dobsm(3),dx,beta,spekcut,ebeam,sbnor,speknor,curr,debeam,
     &  di0,dd0,rpin,ppin,pw,ph,pr,pc(3),beff,park,wlen1,eharm1,xlell,parke,
     &  phsum

      double precision dppv(13),dtev(13),dpp2(13),ws1(13),ws2(13),ws3(13),ws4(13)

      double complex, dimension (:), allocatable :: uampx,uampy,uampz
      double precision, dimension (:,:), allocatable :: utraxyz,ustokes

      integer :: kfreq,iobsv,nper,i,np2,nelec,mbunch,meinbunch,ibu,kran,ib0max,k,
     &  ifail,ndimu,nstepu,l,ith,noespread,noemit,jbunch,jubunch,jhbunch,jpola,
     &  jstokes,jeneloss,jvelofield,jcharge,lmodeph

      integer :: idebug=-1

      character(3) cmodph

*KEEP,datetime.
      include 'datetime.cmn'
*KEEP,contrl.
      include 'contrl.cmn'
*KEEP,cmpara.
      include 'cmpara.cmn'
*KEEP,track.
      include 'track.cmn'
*KEEP,track0.
      include 'track0.cmn'
*KEEP,spect.
      include 'spect.cmn'
*KEEP,observ.
      include 'observ.cmn'
*KEEP,sourcef90.
      include 'sourcef90.cmn'
*KEEP,freqs.
      include 'freqs.cmn'
*KEEP,ampli.
      include 'ampli.cmn'
*KEEP,wfoldf90.
      include 'wfoldf90.cmn'
*KEEP,optic.
      include 'optic.cmn'
*KEEP,ellip.
      include 'ellip.cmn'
*KEEP,b0scglob.
      include 'b0scglob.cmn'
*KEEP,depola.
      include 'depola.cmn'
*KEEP,phycon.
      include 'phycon.cmn'
*KEEP,uservar.
      include 'uservar.cmn'
*KEND.

      call date_and_time(dtday,dttime,dtzone,idatetime)
      call iutil_date_time

      if (idebug.gt.0) call util_break

      write(6,*)
      write(6,*)'     Starting calculations in AMPREP_nobsv_omp: '
     &  ,dttime(1:2),':',dttime(3:4),':',dttime(5:6)
      write(6,*)

      dr00=[1.0d0,0.0d0,0.0d0]
      drn00=dr00/norm2(dr00)
      dr00=drn00*phperl
      r00=[0.0d0,0.0d0,0.0d0]

      perlen=phperl

      vxi=vx0
      vyi=vy0
      vzi=vz0

      r0=r00
      dr0=dr00
      drn0=drn00
      r=r0
      drn=drn0

      vf0=norm2([vxf0,vyf0,vzf0])
      efx=vxf0/vf0
      efy=vyf0/vf0
      efz=vzf0/vf0

      ds=dtim0*vf0
      ndimu=nco*1.1

      r0=[x0,y0,z0]
      dr0=[xf0-x0,yf0-y0,zf0-z0]
      dr0=[efx,efy,efz]*perlen
      r0=r0+dr0/2.0d0

      allocate(ecmx(nsource),frq(nfreq),uampx(nfreq),uampy(nfreq),uampz(nfreq),
     &  utraxyz(14,ndimu),ustokes(4,nfreq),spow(nobsv))

      if (nfreq.eq.1) then
        flow=freq(1)
        fhigh=freq(1)
      else
        flow=freqlow
        fhigh=freqhig
      endif

      beff=sqrt(bymx**2+bzmx**2)
      park=echarge1*beff*perlen/(2.*pi1*emasskg1*clight1)
      wlen1=(1+park**2/2.)/2./dmygamma**2*perlen*1.0d9

      if (wlen1.ne.0.0) then
        eharm1=wtoe1/wlen1
      else
        eharm1=0.0d0
      endif

      dtpho=perlen/clight1

      nper=iabs(kampli)

      allocate(pherrc(nper),pherr(nper),affe(3,nfreq*nobsv),phiran(max(1,nbunch)),
     &  wsspec(nfreq*nobsv),wsstokes(4,nfreq*nobsv))

      np2=nper/2

      affe=(0.0D0,0.0D0)
      spec=0.0d0
      stokes=0.0d0

      call util_random_gauss_omp(nper,pherr,rr)
      pherrc=pherr

      lmodeph=modeph

      if (idebug.gt.0) call util_break

      if (lmodeph.eq.0.and.eharm1.ne.0.0d0) then
        om1=eharm1/hbarev1
        pherr=sngl(pherrc*pherror/360.0d0*twopi1/om1)
      else if (lmodeph.eq.1) then
        pherr=sngl(pherr*pherror)
      else if (lmodeph.eq.2) then
        pherr(nper)=0.0d0
        phsum=0.0d0
        do i=1,nper-1
          pherr(i)=pherr(i)+pherrc(i)
          pherr(i+1)=pherr(i+1)-pherrc(i)
          phsum=phsum+pherr(i)
        enddo
        phsum=phsum+pherr(nper)
      else
        pherr=0.0
      endif !(lmodeph.eq.0)

      write(lungfo,*)'      '
      write(lungfo,*)'      Subroutine amprep_nobsv_omp called to sum up radiation of a single period '
      write(lungfo,*)'      '
      write(lungfo,*)'       NOEMITPH, NOESPREADPH:',NOEMITPH,NOESPREADPH
      write(lungfo,*)'       PHPERL, PHSHIFT:',SNGL(PHPERL),SNGL(PHSHIFT)
      write(lungfo,*)'       PHB0H, PHB0V:',SNGL(PHB0H),SNGL(PHB0V)
      write(lungfo,*)'       MODEPH, KAMPLI:',modeph,kampli
      write(lungfo,*)'       PHERROR, Sum of phase errors: ',sngl(pherror),sngl(phsum)
      write(lungfo,*)'      '

      mbunch=max(1,nbunch)
      meinbunch=max(1,neinbunch)
      nelec=max(1,mbunch*meinbunch)

      if (ibunch.ne.0.and.meinbunch.ne.1) then
        stop "*** Error in AMPREP_nobsv_omp: Neinbunch not one is not yet implemented ***"
      endif

      if (ibunch.ne.0.and.bunchcharge.ne.0.0d0) then
        sqnbunch=mbunch
        sqnphsp=sqrt(bunchcharge/echarge1)
     &    *meinbunch
     &    /(bunchcharge/echarge1)
        bunnor=1.0d0/mbunch
      else
        sqnbunch=mbunch
        sqnphsp=sqrt(dble(meinbunch))
        bunnor=1.0d0/mbunch
      endif

      phiran=0.0
      if (nbunch.gt.0) call util_random(nbunch,phiran)
      phiran=phiran*twopi1
      if (ibunch.eq.-1) phiran(1)=0.0

      sigh=bsigz(1)
      sigph=bsigzp(1)
      sigv=bsigy(1)
      sigpv=bsigyp(1)

      if (ibunch.ne.0) then
        if (iubunch.eq.1) then
          alphah=-betaph/2.0d0
          gammah=(1.0d0+alphah**2)/betah
          beta0h=1.0d0/gammah
          s0h=alphah/gammah
          alphav=-betapv/2.0d0
          gammav=(1.0d0+alphav**2)/betav
          beta0v=1.0d0/gammav
          s0v=alphav/gammah
          sigh=sqrt(eps0h*beta0h)
          sigph=sqrt(eps0h/beta0h)
          sigv=sqrt(eps0v*beta0v)
          sigpv=sqrt(eps0v/beta0v)
        else if (iubunch.ne.-1.and.iubunch.ne.0.and.
     &      (noemitph.eq.0.or.noespreadph.ne.0)) then
          stop "*** Error in AMPREP_nobsv_omp: IUBUNCH must be one of [0,1,-1] ***"
        endif
      endif

      ielec=0

      frq=freq(:nfreq)
      spow=specpow(:nobsv)
      ecmx=ecmax(:nsource)
      noemit=noemitph
      noespread=noespreadph
      jbunch=ibunch
      jubunch=iubunch
      jhbunch=max(0,ihbunch)
      if (jhbunch.ne.0) then
          jhbunch=max(1,jhbunch/mampthreads)
      endif
      jubunch=iubunch
      jstokes=istokes
      spekcut=speccut
      ebeam=dmyenergy
      debeam=espread
      di0=disp0
      dd0=ddisp0
      curr=dmycur
      refl=reflec
      jpola=ipola
      wpola=vpola
      stokesv=vstokes
      sbnor=specnor*bunnor
      speknor=specnor
      jeneloss=ieneloss
      jvelofield=ivelofield
      jcharge=icharge
      pw=pinw
      ph=pinh
      pr=pinr
      pc=pincen
      beff=sqrt(phb0v**2+phb0h**2)
      parke=echarge1*beff*phperl/(2.*pi1*emasskg1*clight1)
      xlell=phperl

      do ibu=1,nelec

        isub=1 ! später Schleife über neinbunch
        ibun=ibu

        wsspec=0.0d0
        wsstokes=0.0d0

c        ielec=ielec+1
        ielec=ibu

        phbu=cdexp(dcmplx(0.0d0,phiran(ibu)))
        bunchx=0.0d0

        xi=x0
        yi=y0
        zi=z0

        zpi=vz0/vx0
        ypi=vy0/vx0

        x2=xf0
        y2=yf0
        z2=zf0

        vx2=vxf0
        vy2=vyf0
        vz2=vzf0

        dpp=0.0d0
        gamma=dmygamma

        xi=x0
        yi=y0
        zi=z0

        ypi=vy0/vx0
        zpi=vz0/vx0

        vxi=vx0
        vyi=vy0
        vzi=vz0

        if (idebug.gt.0) call util_break

        if ((noespread.eq.0.or.noemit.eq.0).and.
     &      jbunch.ne.0.and.(jbunch.ne.-1.or.ibu.gt.1)) then

          if (noespread.eq.0) kran=1
          if (noemit.eq.0) kran=5

          call util_random_gauss_omp(kran,xran,rr)

          if (iamppin.eq.3) then
            call util_random(2,pran)
            obs(1)=pc(1)
            if (iamppincirc.eq.0) then
              obs(2)=pc(2)+(pran(1)-0.5)*pw
              obs(3)=pc(3)+(pran(2)-0.5)*ph
            else
              rpin=(pran(1)-0.5)*pr
              ppin=pran(2)*twopi1
              obs(2)=pc(2)+rpin*cos(ppin)
              obs(3)=pc(3)+rpin*sin(ppin)
            endif
          endif

          if (noespread.eq.0) then
            dpp=debeam*xran(1)
            gamma=(1.0d0+dpp)*dmygamma
          endif

          ! assume beta(s)=beta0(s)+s**2/beta(0) and alpha0=-s/beta(0)
          ! and a drift transfer-matrix ((1,s),(1,0))

          if (noemit.eq.0) then

            if (jubunch.eq.0) then

              zz=sigh*xran(2)
              zzp=sigph*xran(3)

              zi=zz-x0*zzp !inverse transformation
              zpi=zzp

              yy=sigv*xran(4)
              yyp=sigpv*xran(5)

              yi=yy-x0*yyp
              ypi=yyp

            else if (jubunch.eq.1) then

              zz=sigh*xran(2)
              zzp=sigph*xran(3)

              zi=zz-s0h*zzp !inverse transformation
              zpi=zzp

              yy=sigv*xran(4)
              yyp=sigpv*xran(5)

              yi=yy-s0v*yyp
              ypi=yyp

            endif !(noemit.eq.0)

          else if (jubunch.eq.-1) then

            call ubunch(xi,yi,zi,ypi,zpi,gamma,dt)
            dpp=(gamma-dmygamma)/dmygamma
            gamma=(1.0d0+dpp)*dmygamma

          endif !jubunch

          ! simple treatment of closed orbit, assume small angles

          zi=zi+z0
          zpi=zpi+vz0/vx0

          yi=yi+y0
          ypi=ypi+vy0/vx0

          vn=clight1*dsqrt((1.0d0-1.0d0/gamma)*(1.0d0+1.0d0/gamma))

          zi=zi+dpp*di0
          zpi=zpi+dpp*dd0

          vxi=vn/sqrt(1.0d0+ypi**2+zpi**2)
          vyi=vxi*ypi
          vzi=vxi*zpi

        else

          xi=x0
          yi=y0
          zi=z0

          ypi=vy0/vx0
          zpi=vz0/vx0

          vxi=vx0
          vyi=vy0
          vzi=vz0

        endif !(jbunch.ne.-1.or.ielec.ne.1)

        if (idebug.gt.0) call util_break

!$OMP PARALLEL NUM_THREADS(mampthreads) DEFAULT(PRIVATE)
!$OMP& FIRSTPRIVATE(nfreq,nobsv,nelec,frq,nper,np2,perlen,clight1,hbarev1,flow,fhigh,
!$OMP& x0,y0,z0,xf0,yf0,zf0,vx0,vy0,vz0,vxf0,vyf0,vzf0,dmygamma,sbnor,speknor,
!$OMP& efx,efy,efz,ds,ndimu,jcharge,jeneloss,jvelofield,curr,gamma,
!$OMP& jbunch,jubunch,jhbunch,noespread,noemit,spekcut,ecdipev1,ebeam,refl,jpola,wpola,
!$OMP& stokesv,ecmx,icbrill,obsv,emassg1,jstokes,nidbunch,debeam,sigh,sigph,
!$OMP& xi,yi,zi,vxi,vyi,vzi,xlell,parke,lmodeph,
!$OMP& sigv,sigpv,pran,xran,di0,dd0,fillb,r0,dr0,iamppin,iamppincirc,obs,
!$OMP& idebug,wsspec,wsstokes)
!$OMP& SHARED(mampthreads,affe,spec,specpow,stokes,ielec,phiran,ibun,ibu,pherr)

!$OMP DO

        do iobsv=1,nobsv

          ith=OMP_GET_THREAD_NUM()+1

          if (idebug.eq.-1.and.ith.eq.1) call util_break

          obs=obsv(1:3,iobsv)

          if (iamppin.eq.3) then
            stop "*** ERROR IN AMPREP_NOBSV_OMP: IPIN=3 NOT ALLOWED!!!"
           endif

          vn=norm2([vxi,vyi,vzi])
          eix=vxi/vn
          eiy=vyi/vn
          eiz=vzi/vn

          call urad_omp(jcharge,
     &      gamma,udgamtot,
     &      xi,yi,zi,vxi,vyi,vzi,
     &      xf0,yf0,zf0,efx,efy,efz,
     &      x2,y2,z2,vx2,vy2,vz2,dtelec,ds,
     &      0,nstepu,ndimu,utraxyz,
     &      obs(1),obs(2),obs(3),flow,fhigh,
     &      nfreq,frq,uampx,uampy,uampz,ustokes,upow,
     &      jeneloss,jvelofield,ifail,ith)

          r0=[xi,yi,zi]
          dr0=[x2-xi,y2-yi,z2-zi]
          r0=r0+dr0/2.0d0

          if (ibu.eq.1) then
            pow=specpow(iobsv)
            specpow(iobsv)=0.0d0
          endif

          do kfreq=1,nfreq

            ifrob=kfreq+nfreq*(iobsv-1)
            iobfr=iobsv+nobsv*(kfreq-1)

            om=frq(kfreq)/hbarev1
            amp0=[uampx(kfreq),uampy(kfreq),uampz(kfreq)]*1.0d3/sqrt(speknor/curr*0.10d0) !urad

            amp=(0.0d0,0.0d0)
            t=0.0d0

            do i=1,nper

              r=r0+(i-np2-1)*dr0
              dobs=obs-r
              dist0=norm2(obs-r0)
              dist=norm2(dobs)

              if (lmodeph.eq.0) then
                dt=xlell/clight1*((1.0d0+parke**2/2.0d0)/2.0d0/gamma**2+
     &            (((ypi-dobs(2)/dobs(1))**2+(zpi-dobs(3)/dobs(1))**2))/2.0d0)
                t=t+dt
                dph=om*(t+pherr(i))
              else if (lmodeph.eq.1.or.lmodeph.eq.2) then
                pkerr=parke*(1.0d0+pherr(i))
                dt=xlell/clight1*((1.0d0+pkerr**2/2.0d0)/2.0d0/gamma**2+
     &            (((ypi-dobs(2)/dobs(1))**2+(zpi-dobs(3)/dobs(1))**2))/2.0d0)
                t=t+dt
                dph=om
              endif !lmodeph

              zexp=cdexp(dcmplx(0.0d0,dph))
              damp=amp0*zexp*dist0/dist
              amp=amp+damp

              if (jhbunch.ne.0) then
                if (iobsv.eq.icbrill.and.(ibu.eq.1.or.mod(ibu,jhbunch).eq.0)) then

                  if (i.eq.1) then
                    fillb(5)=r(1)
                    fillb(6)=r(2)
                    fillb(7)=r(3)
                    fillb(8)=ypi
                    fillb(9)=zpi
                  else if (i.eq.nper) then
                    fillb(10:12)=r+dr
                    fillb(13)=ypi
                    fillb(14)=zpi
                  endif

                endif
              endif

              if (kfreq.eq.1.and.ibu.eq.1)
     &          specpow(iobsv)=specpow(iobsv)+pow*(dist0/dist)**2

            enddo !nper

            if(spekcut.gt.0.0d0) then
              if(frq(kfreq).gt.spekcut*ecdipev1*ebeam**2*ecmx(1)) then
                amp=(0.0d0,0.0d0)
              endif
            endif

            amp=amp*refl

            if (jpola.eq.0) then
              speck=sum(amp*conjg(amp))*sbnor
            else    !jpola
              apol=
     &           amp(1)*conjg(wpola(1))
     &          +amp(2)*conjg(wpola(2))
     &          +amp(3)*conjg(wpola(3))
              speck=dreal(apol*conjg(apol))*sbnor
            endif   !jpola

            wsspec(iobfr)=wsspec(iobfr)+speck

            if (jstokes.ne.0) then

              apolh=
     &          amp(1)*conjg(stokesv(1,1))
     &          +amp(2)*conjg(stokesv(1,2))
     &          +amp(3)*conjg(stokesv(1,3))

              apolr=
     &          amp(1)*conjg(stokesv(2,1))
     &          +amp(2)*conjg(stokesv(2,2))
     &          +amp(3)*conjg(stokesv(2,3))

              apoll=
     &          amp(1)*conjg(stokesv(3,1))
     &          +amp(2)*conjg(stokesv(3,2))
     &          +amp(3)*conjg(stokesv(3,3))

              apol45=
     &          amp(1)*conjg(stokesv(4,1))
     &          +amp(2)*conjg(stokesv(4,2))
     &          +amp(3)*conjg(stokesv(4,3))

              stok1=apolr*conjg(apolr)+apoll*conjg(apoll)
              stok2=-stok1+2.0d0*apolh*conjg(apolh)
              stok3=2.0d0*apol45*conjg(apol45)-stok1
              stok4=apolr*conjg(apolr)-apoll*conjg(apoll)

              wsstokes(1,iobfr)=wsstokes(1,iobfr)+stok1*sbnor
              wsstokes(2,iobfr)=wsstokes(2,iobfr)+stok2*sbnor
              wsstokes(3,iobfr)=wsstokes(3,iobfr)+stok3*sbnor
              wsstokes(4,iobfr)=wsstokes(4,iobfr)+stok4*sbnor

            endif !jstokes

            spec(iobfr)=spec(iobfr)+speck
            stokes(1:4,iobfr)=stokes(1:4,iobfr)+wsstokes(1:4,iobfr)
            affe(:,ifrob)=affe(:,ifrob)+phbu*amp

            if (jhbunch.ne.0) then
              if (iobsv.eq.icbrill.and.(ibu.eq.1.or.mod(ibu,jhbunch).eq.0)) then

                fillb(1)=ibun
                fillb(2)=isub
                fillb(3)=ibu
                fillb(4)=bunchx
                fillb(15)=gamma*emassg1
                fillb(16)=(gamma+udgamtot)*emassg1
                fillb(17)=obsv(1,iobsv)
                fillb(18)=obsv(2,iobsv)
                fillb(19)=obsv(3,iobsv)
                fillb(20)=kfreq
                fillb(21)=frq(kfreq)
                fillb(22)=wsspec(iobfr)*nelec

                if (jstokes.ne.0) then
                  fillb(23)=wsstokes(1,iobfr)*nelec
                  fillb(24)=wsstokes(2,iobfr)*nelec
                  fillb(25)=wsstokes(3,iobfr)*nelec
                  fillb(26)=wsstokes(4,iobfr)*nelec
                else
                  fillb(23)=fillb(22)
                  fillb(24:26)=0.0d0
                endif !jstokes

                fillb(27)=specpow(iobsv)
                fillb(28)=1
                fillb(29)=dtelec

                call hfm(nidbunch,fillb)

              endif
            endif

          enddo !kfreq

        enddo !nobsv

!$OMP END DO
!$OMP END PARALLEL

      enddo !nbunch

      do iobsv=1,nobsv
        do kfreq=1,nfreq
          ifrob=kfreq+nfreq*(iobsv-1)
          iobfr=iobsv+nobsv*(kfreq-1)
          reaima(1:3,1,iobfr)=dreal(affe(1:3,ifrob))/sqnbunch
          reaima(1:3,2,iobfr)=dimag(affe(1:3,ifrob))/sqnbunch
        enddo !kfreq
      enddo !nobsv

      call date_and_time(dtday,dttime,dtzone,idatetime)

      write(6,*)
c      write(6,*)'     Finishing calculations in AMPREP_nobsv_omp with  MODEPH =' // cmodph //': '
      write(6,*)'     Finishing calculations in AMPREP_nobsv_omp: '
     &  ,dttime(1:2),':',dttime(3:4),':',dttime(5:6)
      write(6,*)

      deallocate(ecmx,frq,uampx,uampy,uampz,utraxyz,ustokes,spow)
      deallocate(pherr,pherrc,affe,phiran,wsspec,wsstokes)

      return
      end
