*CMZ :  4.00/13 28/10/2021  13.59.10  by  Michael Scheer
*CMZ :  3.05/04 02/07/2018  17.36.21  by  Michael Scheer
*CMZ :  3.04/00 24/01/2018  13.28.04  by  Michael Scheer
*CMZ :  3.03/02 16/02/2016  12.18.47  by  Michael Scheer
*CMZ :  3.01/00 15/07/2013  08.04.32  by  Michael Scheer
*CMZ :  2.68/03 07/08/2012  13.09.30  by  Michael Scheer
*CMZ :  2.66/07 04/12/2009  16.11.19  by  Michael Scheer
*CMZ :  2.52/11 15/03/2007  11.13.54  by  Michael Scheer
*CMZ :  2.15/00 28/04/2000  10.32.33  by  Michael Scheer
*CMZ :  1.02/00 19/12/97  16.15.53  by  Michael Scheer
*CMZ :  1.01/00 28/10/97  12.14.09  by  Michael Scheer
*CMZ : 00.01/08 01/04/95  16.54.24  by  Michael Scheer
*CMZ : 00.01/07 10/03/95  11.22.55  by  Michael Scheer
*CMZ : 00.01/02 04/11/94  15.21.20  by  Michael Scheer
*CMZ : 00.00/04 29/04/94  17.48.03  by  Michael Scheer
*CMZ : 00.00/00 28/04/94  16.13.42  by  Michael Scheer
*-- Author : Michael Scheer
      SUBROUTINE uradBMAGSEQC(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,AXOUT,AYOUT,AZOUT)
*KEEP,gplhint.
!******************************************************************************
!
!      Copyright 2013 Helmholtz-Zentrum Berlin (HZB)
!      Hahn-Meitner-Platz 1
!      D-14109 Berlin
!      Germany
!
!      Author Michael Scheer, Michael.Scheer@Helmholtz-Berlin.de
!
! -----------------------------------------------------------------------
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy (wave_gpl.txt) of the GNU General Public
!    License along with this program.
!    If not, see <http://www.gnu.org/licenses/>.
!
!    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
!    der GNU General Public License, wie von der Free Software Foundation,
!    Version 3 der Lizenz oder (nach Ihrer Option) jeder spaeteren
!    veroeffentlichten Version, weiterverbreiten und/oder modifizieren.
!
!    Dieses Programm wird in der Hoffnung, dass es nuetzlich sein wird, aber
!    OHNE JEDE GEWAEHRLEISTUNG, bereitgestellt; sogar ohne die implizite
!    Gewaehrleistung der MARKTFAEHIGKEIT oder EIGNUNG FueR EINEN BESTIMMTEN ZWECK.
!    Siehe die GNU General Public License fuer weitere Details.
!
!    Sie sollten eine Kopie (wave_gpl.txt) der GNU General Public License
!    zusammen mit diesem Programm erhalten haben. Wenn nicht,
!    siehe <http://www.gnu.org/licenses/>.
!
!******************************************************************************
*KEND.

      IMPLICIT NONE

      INTEGER IM,ISTORE

*KEEP,uradcom.
      integer nfour,nfourwls,ifour0,iprntf,maxfoumagp,nfoumags,nfoumagcp
      parameter (maxfoumagp=100,nfoumagcp=2**10)

      double precision
     &  xfoubounds(5,maxfoumagp),foumags(nfoumagcp/2+3,maxfoumagp)
     &  ,fouentr,fouexit,xshbfour

c      character(2048) chfoumags(maxfoumagp)

      integer kmonopole,intpolbmap,kbmap,kmagseq,kbmapu,kmagsequ

      double precision xlenfour,xbhomf,dbhomf,emom
      character(2048) fmagseq

      common/cfourier/
     &  xlenfour,xbhomf,dbhomf,emom
     &  ,xfoubounds,foumags
     &  ,fouentr,fouexit,xshbfour
     &  ,kmagseq,fmagseq
     &  ,nfour,nfourwls,ifour0,iprntf,nfoumags,kmonopole

      integer iahwfour,nhhalbasy

      double precision b0scale,hshift,vshift

      integer khalbasy

      common/bscalec/ b0scale,hshift,vshift,intpolbmap

      double precision b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur

      common/uradhalbasym/
     &  b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur,
     &  khalbasy,iahwfour

      integer nmgsqp,mmag,irfilf,iwfilf
      parameter(nmgsqp=10000)

      character(3) ctyp(nmgsqp)

      double precision pmag(13,nmgsqp),coz(2,nmgsqp+1)
     &  ,corr(nmgsqp),uebounds(2,nmgsqp),
     &  dibounds(2,nmgsqp),
     &  dhbounds(2,nmgsqp),
     &  qfbounds(2,nmgsqp),
     &  sxbounds(2,nmgsqp),
     &  bmsqbounds(2)

      common/uradmgsqc/pmag,coz,corr,dibounds,dhbounds,
     &  uebounds,qfbounds,sxbounds,bmsqbounds,mmag,irfilf,iwfilf,
     &  kbmapu,kmagsequ,
     &  ctyp

      namelist/ufield/nfour,nfourwls,ifour0,xlenfour,dbhomf,iprntf,irfilf,
     &  b0halbasy,xlhalbasy,ylhalbasy,hhalbasy,pkhalbasy,
     &  zlhalbasy,fasym,ahwpol,iahwfour,xcenhal,nhhalbasy,
     &  b0scale,hshift,vshift,kmonopole,intpolbmap,kbmap,kmagseq,fmagseq
*KEND.

      DOUBLE PRECISION BX,BY,BZ,BXOUT,BYOUT,BZOUT,AXOUT,AYOUT,AZOUT
      DOUBLE PRECISION XIN,YIN,ZIN,xlen2

      integer ical
      save ical

      data ical/0/

      if (ical.eq.0) then

        bmsqbounds(1)=1.0d30
        bmsqbounds(2)=-1.0d30

        do im=1,mmag

          if (ctyp(im).eq.'QP') then
            xlen2=pmag(1,im)/2.0d0
            qfbounds(1,im)=pmag(3,im)-xlen2
            qfbounds(2,im)=pmag(3,im)+xlen2
            if (qfbounds(2,im).lt.qfbounds(1,im)) then
              write(6,*)'*** Error in URADBMAGSEQC: Bounderies of QP ',im,'bad! ***'
              stop '*** Program WAVE aborted ***'
            endif
            if (qfbounds(1,im).lt.bmsqbounds(1)) bmsqbounds(1)=qfbounds(1,im)
            if (qfbounds(2,im).gt.bmsqbounds(2)) bmsqbounds(2)=qfbounds(2,im)
          else if (ctyp(im).eq.'QF') then
            xlen2=pmag(1,im)/2.0d0
            qfbounds(1,im)=pmag(3,im)-70.0d0/1000.0d0-xlen2
            qfbounds(2,im)=pmag(3,im)+70.0d0/1000.0d0+xlen2
            if (qfbounds(2,im).lt.qfbounds(1,im)) then
              write(6,*)'*** Error in URADBMAGSEQC: Bounderies of QF ',im,'bad! ***'
              stop '*** Program WAVE aborted ***'
            endif
            if (qfbounds(1,im).lt.bmsqbounds(1)) bmsqbounds(1)=qfbounds(1,im)
            if (qfbounds(2,im).gt.bmsqbounds(2)) bmsqbounds(2)=qfbounds(2,im)
          else if (ctyp(im).eq.'SX') then
            xlen2=pmag(1,im)/2.0d0
            sxbounds(1,im)=pmag(3,im)-70.0d0/1000.0d0-xlen2
            sxbounds(2,im)=pmag(3,im)+70.0d0/1000.0d0+xlen2
            if (sxbounds(2,im).lt.sxbounds(1,im)) then
              write(6,*)'*** Error in URADBMAGSEQC: Bounderies of SX ',im,'bad! ***'
              stop '*** Program WAVE aborted ***'
            endif
            if (sxbounds(1,im).lt.bmsqbounds(1)) bmsqbounds(1)=sxbounds(1,im)
            if (sxbounds(2,im).gt.bmsqbounds(2)) bmsqbounds(2)=sxbounds(2,im)
          else if (ctyp(im).eq.'DI'.or.ctyp(im).eq.'DIF') then
            xlen2=dabs(pmag(2,im)*sin(pmag(1,im)/2.0d0))
            dibounds(1,im)=pmag(3,im)-70.0d0/pmag(4,im)-xlen2
            dibounds(2,im)=pmag(3,im)+70.0d0/pmag(4,im)+xlen2
            if (dibounds(1,im).lt.bmsqbounds(1)) bmsqbounds(1)=dibounds(1,im)
            if (dibounds(2,im).gt.bmsqbounds(2)) bmsqbounds(2)=dibounds(2,im)
          else if (ctyp(im).eq.'DH'.or.ctyp(im).eq.'DHF') then
            xlen2=dabs(pmag(2,im)*sin(pmag(1,im)/2.0d0))
            dhbounds(1,im)=pmag(3,im)-70.0d0/pmag(4,im)-xlen2
            dhbounds(2,im)=pmag(3,im)+70.0d0/pmag(4,im)+xlen2
            if (dhbounds(1,im).lt.bmsqbounds(1)) bmsqbounds(1)=dhbounds(1,im)
            if (dhbounds(2,im).gt.bmsqbounds(2)) bmsqbounds(2)=dhbounds(2,im)
          else if (ctyp(im).eq.'UE') then
            if (uebounds(2,im).lt.uebounds(1,im)) then
              write(6,*)'*** Error in URADBMAGSEQC: Bounderies of UE ',im,'bad! ***'
              stop '*** Program WAVE aborted ***'
            endif
            if (uebounds(1,im).lt.bmsqbounds(1)) bmsqbounds(1)=uebounds(1,im)
            if (uebounds(2,im).gt.bmsqbounds(2)) bmsqbounds(2)=uebounds(2,im)
          endif
        enddo

        ical=1

      endif

      BXOUT=0.0d0
      BYOUT=0.0d0
      BZOUT=0.0d0

      AXOUT=0.0d0
      AYOUT=0.0d0
      AZOUT=0.0d0

      BX=0.0d0
      BY=0.0d0
      BZ=0.0d0

C- FIELD

      DO IM=1,mmag

        IF     (CTYP(IM).EQ.'QP') THEN
          CALL uradBQP(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,IM)
        ELSE IF     (CTYP(IM).EQ.'QF') THEN
          if (xin+1.0d-10.ge.qfbounds(1,im).and.xin-1.0d-10.le.qfbounds(2,im)) then
            CALL uradBQF(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ELSE IF     (CTYP(IM).EQ.'SX') THEN
          if (xin+1.0d-10.ge.sxbounds(1,im).and.xin-1.0d-10.le.sxbounds(2,im)) then
            CALL uradbsx(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ELSE IF     (CTYP(IM).EQ.'DI') THEN
          if (xin+1.0d-10.ge.dibounds(1,im).and.xin-1.0d-10.le.dibounds(2,im)) then
            CALL uradBDI(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ELSE IF     (CTYP(IM).EQ.'DIF') THEN
          if (xin+1.0d-10.ge.dibounds(1,im).and.xin-1.0d-10.le.dibounds(2,im)) then
            CALL uradbfoumag(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,axout,ayout,azout,IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ELSE IF     (CTYP(IM).EQ.'DHF') THEN
          if (xin+1.0d-10.ge.dhbounds(1,im).and.xin-1.0d-10.le.dhbounds(2,im)) then
            CALL uradbfoumag(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,axout,ayout,azout,-IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ELSE IF     (CTYP(IM).EQ.'DH') THEN
          if (xin+1.0d-10.ge.dhbounds(1,im).and.xin-1.0d-10.le.dhbounds(2,im)) then
            CALL uradBDH(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ELSE IF     (CTYP(IM).EQ.'UE') THEN
          if (xin+1.0d-10.ge.uebounds(1,im).and.xin-1.0d-10.le.uebounds(2,im)) then
            CALL uradBUE(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,axout,ayout,azout,IM)
          else
            bxout=0.0d0
            byout=0.0d0
            bzout=0.0d0
          endif
        ENDIF !CTYP

        BX=BX+BXOUT
        BY=BY+BYOUT
        BZ=BZ+BZOUT

      ENDDO !IM

      BXOUT=BX
      BYOUT=BY
      BZOUT=BZ

      RETURN
      END

c      include 'uradbue.f'
c      include 'uradbfoumag.f'
