*CMZ :  3.04/00 23/01/2018  16.43.55  by  Michael Scheer
*CMZ :  3.03/04 19/12/2017  13.16.56  by  Michael Scheer
*CMZ :  3.00/00 11/03/2013  10.38.36  by  Michael Scheer
*CMZ :  2.63/05 12/05/2010  13.34.28  by  Michael Scheer
*CMZ :  2.61/01 15/03/2007  11.13.54  by  Michael Scheer
*CMZ :  2.61/00 30/01/2007  19.54.23  by  Michael Scheer
*-- Author :    Michael Scheer   30/01/2007
      SUBROUTINE urad_IDTRMSHGF(ilinmat,nfour,hshift,vshift,
     &  xco0,yco0,zco0,vnxco0,vnyco0,vnzco0,
     &  xcof,ycof,zcof,vnxcof,vnycof,vnzcof,
     &  icharge,gammai,
     &  xelec,yelec,zelec,vxelec,vyelec,vzelec,
     &  xexit,yexit,zexit,vxexit,vyexit,vzexit,sexit,
     &  istatus
     &  )
*KEEP,gplhint.
!******************************************************************************
!
!      Copyright 2013 Helmholtz-Zentrum Berlin (HZB)
!      Hahn-Meitner-Platz 1
!      D-14109 Berlin
!      Germany
!
!      Author Michael Scheer, Michael.Scheer@Helmholtz-Berlin.de
!
! -----------------------------------------------------------------------
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy (wave_gpl.txt) of the GNU General Public
!    License along with this program.
!    If not, see <http://www.gnu.org/licenses/>.
!
!    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
!    der GNU General Public License, wie von der Free Software Foundation,
!    Version 3 der Lizenz oder (nach Ihrer Option) jeder spaeteren
!    veroeffentlichten Version, weiterverbreiten und/oder modifizieren.
!
!    Dieses Programm wird in der Hoffnung, dass es nuetzlich sein wird, aber
!    OHNE JEDE GEWAEHRLEISTUNG, bereitgestellt; sogar ohne die implizite
!    Gewaehrleistung der MARKTFAEHIGKEIT oder EIGNUNG FueR EINEN BESTIMMTEN ZWECK.
!    Siehe die GNU General Public License fuer weitere Details.
!
!    Sie sollten eine Kopie (wave_gpl.txt) der GNU General Public License
!    zusammen mit diesem Programm erhalten haben. Wenn nicht,
!    siehe <http://www.gnu.org/licenses/>.
!
!******************************************************************************
*KEND.

C--- TRACKING WITH POLYNOMIAL-EXPANSION OF GENERATING FUNCTION

C xelec,yelec,zelec,XF,YF,ZF ARE COORDINATES IN LAB

      IMPLICIT NONE

*KEEP,optic.
      include 'optic.cmn'
*KEEP,genfun.
      include 'genfun.cmn'
*KEND.

      double precision, dimension (:,:,:,:,:), allocatable :: ake

      double precision, dimension (:), allocatable ::
     &  A1100e,A2000e,A0200e,A1000e,A0100e,A0011e,A0020e,A0002e,
     &  qxpow,qypow,pxpow,pypow,sexite,gammae,dgame,
     &  zwfe,zpfe,ywfe,ypfe,
     &  sort,a0scalee,
     &  x0e,y0e,z0e,xf0e,yf0e,zf0e,zp0e,yp0e,zpf0e,ypf0e,
     &  bx0e,by0e,bz0e,ax0e,ay0e,az0e,
     &  bxf0e,byf0e,bzf0e,axf0e,ayf0e,azf0e,
     &  xle,yle,zle,vxfe,vyfe,vzfe,
     &  pxi0e,pyi0e,pxf0e,pyf0e

      double precision, dimension (:,:,:,:), allocatable :: akoeff
      double precision, dimension (:,:,:), allocatable :: tfme
      double precision, dimension (:,:), allocatable :: ewse,ewsfe

      integer, dimension (:), allocatable :: isorte

      double precision
     &  gammai,
     &  xelec,yelec,zelec,vxelec,vyelec,vzelec,
     &  xexit,yexit,zexit,vxexit,vyexit,vzexit,
     &  vnxco0,vnyco0,vnzco0,gamma,dgam,
     &  vnxcof,vnycof,vnzcof,beta,hshift,vshift,sexit,a,p,pp

      DOUBLE PRECISION
     &  EWS(3),EWY(3),EWZ(3),EN,
     &  EWSF(3),EWYF(3),EWZF(3),YW,ZW,ZPW,YPW,SPW,
     &  YPF,ZPF,YACC,X0,Y0,Z0,
     &  V0,vn,VxelecN,VyelecN,VzelecN,
     &  XCO0,YCO0,ZCO0,VXCO0,VYCO0,VZCO0,XCOF,YCOF,ZCOF,VXCOF,VYCOF,VZCOF,
     &  BXF,BYF,BZF,AXF,AYF,AZF,SNENN2,PAX,PAY,DXX,DYY,DFFX,FFX,FFY,DFFY,
     &  BRHOABS,SNENN

      REAL*8 TRANSFM(4,4)
      REAL*8 A1100,A2000,A0200,A1000,A0100,A0011,A0020,A0002
      REAL*8 PXI,QXI,QYI,PYI,PX,PY,QX,QY,DX,DY,DZ,YWF,ZWF,XF,
     &  XL,YL,ZL,RNENN

      REAL*8
     &   XF0,YF0,ZF0
     &  ,ZP0,YP0
     &  ,BX0,BY0,BZ0
     &  ,AX0,AY0,AZ0
     &  ,ZPF0,YPF0
     &  ,BXF0,BYF0,BZF0
     &  ,AXF0,AYF0,AZF0

      REAL*8 AXR0,AYR0,AZR0
      REAL*8 AXR,AYR,AZR
      REAL*8 BXI,BYI,BZI
      REAL*8 AXI,AYI,AZI
      REAL*8 AXRF0,AYRF0,AZRF0,A0SCALE,PEL

      INTEGER ICAL,LUNCOE,JCHARGE,MORDNG,JMAX,IREAD,JWRITE,IWRITE,
     &  I,J,K,L,JLOOP,IZAPER,IYAPER,nread,
     &  I1,J1,K1,L1,ilinmat,nfour,
     &  I2,J2,K2,L2,IPOW,nkoef,istatus,icharge,ifile,ieof,mode,ifail,lfile

      integer :: iwarna=0, nfiles=0

      CHARACTER*64 COEFILE
      CHARACTER*65 CODETRA

      DATA LUNCOE/89/
      DATA ICAL/0/

      DATA JMAX/20/,YACC/1.D-10/

*KEEP,phycon.
      include 'phycon.cmn'
*KEND.

      save

C--- INITIALIZATION {

      istatus=0

      IF (ICAL.EQ.0) THEN

        call util_phycon

        BETA=DSQRT((1.0D0-1.0D0/GAMMAI)*(1.0D0+1.0D0/GAMMAI))

        vxco0=clight1*beta*vnxco0
        vyco0=clight1*beta*vnyco0
        vzco0=clight1*beta*vnzco0
        vxcof=clight1*beta*vnxcof
        vycof=clight1*beta*vnycof
        vzcof=clight1*beta*vnzcof

        PEL=EMASSE1*DSQRT( (gammai+1.0D0)*(gammai-1.0D0) )
        BRHOABS=PEL/CLIGHT1  !ABSOLUT VALUE

        WRITE(6,*)
        WRITE(6,*)'      URAD_IDTRMSHGF:'
        WRITE(6,*)

        COEFILE='urad_genfun.cof'

C Einlesen der Koeeficenten  GF(i,j) !!!

        OPEN(UNIT=LUNCOE,FILE = COEFILE,STATUS ='OLD')

1       IREAD=0

        nfiles=nfiles+1

        READ(LUNCOE,'(1A65)')CODETRA
        READ(LUNCOE,*)JCHARGE,MORDNG

        if (nfiles.eq.1) then
          allocate(akoeff(mordng,mordng,mordng,mordng))
          allocate(qxpow(mordng+1),qypow(mordng+1),
     &      pxpow(mordng+1),pypow(mordng+1))
          qxpow=0.0d0
          qypow=0.0d0
          pxpow=0.0d0
          pypow=0.0d0
          qxpow(1)=1.0d0
          qypow(1)=1.0d0
          pxpow(1)=1.0d0
          pypow(1)=1.0d0
        endif

        IF (JCHARGE.NE.ICHARGE) THEN
          WRITE(6,*)'*** ERROR IN urad_idtrmshgf:  ***'
          WRITE(6,*)'GENERATING FUNCTION REFERES TO CHARGE',JCHARGE
          STOP '*** PROGRAM WAVE ABORTED ***'
        ENDIF !JCHARGE

        READ(LUNCOE,*)ZAPERT,YAPERT,DLAPER,sexit,gamma,dgam

        READ(LUNCOE,*)X0,Y0,Z0
     &    ,ZP0,YP0
     &    ,BX0,BY0,BZ0
     &    ,AX0,AY0,AZ0

        READ(LUNCOE,*)XF0,YF0,ZF0
     &    ,ZPF0,YPF0
     &    ,BXF0,BYF0,BZF0
     &    ,AXF0,AYF0,AZF0

        READ(LUNCOE,*)EWS(1),EWS(2),
     &    EWS(3)
        READ(LUNCOE,*)EWSF(1),EWSF(2),
     &    EWSF(3)
        READ(LUNCOE,*)A0SCALE

11      CONTINUE

        call util_skip_comment_end(luncoe,ieof)
        if (ieof.ne.0) goto 9911
        READ (LUNCOE,*,ERR=9911) I,J,K,L,a
        AKOEFF(I+1,J+1,K+1,L+1)=a

        IF(I+1.GT.mordng.OR.J+1.GT.mordng
     &      .OR.K+1.GT.mordng.OR.L+1.GT.mordng) THEN
          WRITE(6,*)
          WRITE(6,*)'*** ERROR SR urad_idtrmshgf ***'
          WRITE(6,*)'ORDER OF COEFFICIENTS ON FILE'
          WRITE(6,*)COEFILE
          WRITE(6,*)
     &      'TOO HIGH INCREASE PARAMETER mordng IN GENFUN.CMN'
          WRITE(6,*)
          WRITE(6,*)
          WRITE(6,*)'*** ERROR SR urad_idtrmshgf ***'
          WRITE(6,*)'ORDER OF COEFFICIENTS ON FILE'
          WRITE(6,*)COEFILE
          WRITE(6,*)
     &      'TOO HIGH INCREASE PARAMETER mordng IN GENFUN.CMN'
          WRITE(6,*)
          STOP
        ENDIF
        IREAD=IREAD+1
        GOTO 11

9911    CONTINUE

       if (ieof.eq.0) then
         nread=iread
         backspace(luncoe)
         goto 1
       endif

       rewind(luncoe)

       allocate(ake(mordng,mordng,mordng,mordng,nfiles))
       allocate(tfme(4,4,nfiles))
       allocate(sexite(nfiles))
       allocate(ewse(3,nfiles),ewsfe(3,nfiles))

       allocate(gammae(nfiles),dgame(nfiles),sort(nfiles),
     &   a1100e(nfiles),a2000e(nfiles),a0200e(nfiles),a1000e(nfiles),
     &   a0100e(nfiles),a0011e(nfiles),a0020e(nfiles),a0002e(nfiles))

       allocate(zwfe(nfiles),zpfe(nfiles),ywfe(nfiles),ypfe(nfiles),
     &   isorte(nfiles),
     &   a0scalee(nfiles),
     &   x0e(nfiles),y0e(nfiles),z0e(nfiles),xf0e(nfiles),
     &   yf0e(nfiles),zf0e(nfiles),zp0e(nfiles),yp0e(nfiles),
     &   xle(nfiles),yle(nfiles),zle(nfiles),
     &   vxfe(nfiles),vyfe(nfiles),vzfe(nfiles),
     &   zpf0e(nfiles),ypf0e(nfiles),
     &   bx0e(nfiles),by0e(nfiles),bz0e(nfiles),
     &   ax0e(nfiles),ay0e(nfiles),az0e(nfiles),
     &   bxf0e(nfiles),byf0e(nfiles),bzf0e(nfiles),
     &   axf0e(nfiles),ayf0e(nfiles),azf0e(nfiles),
     &   pxi0e(nfiles),pyi0e(nfiles),
     &   pxf0e(nfiles),pyf0e(nfiles)
     &   )

       do ifile=1,nfiles

         READ(LUNCOE,'(1A65)')CODETRA
         READ(LUNCOE,*)JCHARGE,MORDNG

         IF (JCHARGE.NE.ICHARGE) THEN
           WRITE(6,*)'*** ERROR IN urad_idtrmshgf:  ***'
           WRITE(6,*)'GENERATING FUNCTION REFERES TO CHARGE',JCHARGE
           STOP '*** PROGRAM WAVE ABORTED ***'
         ENDIF !JCHARGE

         IF (MORDNG.GT.mordng) THEN
           WRITE(6,*)'*** ERROR IN urad_idtrmshgf:  ***'
           WRITE(6,*)'*** DIMENSION mordng EXCEEDED'
           WRITE(6,*)
     &       'TOO HIGH! INCREASE PARAMETER mordng IN GENFUN.CMN'
           WRITE(6,*)'*** ERROR IN urad_idtrmshgf:  ***'
           WRITE(6,*)'*** DIMENSION mordng EXCEEDED'
           WRITE(6,*)
     &       'TOO HIGH! INCREASE PARAMETER mordng IN GENFUN.CMN'
           STOP '*** PROGRAM WAVE ABORTED ***'
         ENDIF

         READ(LUNCOE,*)ZAPERT,YAPERT,DLAPER,sexite(ifile),
     &     gammae(ifile),dgame(ifile)
         isorte(ifile)=ifile

         READ(LUNCOE,*)X0e(ifile),Y0e(ifile),Z0e(ifile)
     &     ,ZP0e(ifile),YP0e(ifile)
     &     ,BX0e(ifile),BY0e(ifile),BZ0e(ifile)
     &     ,AX0e(ifile),AY0e(ifile),AZ0e(ifile)

         READ(LUNCOE,*)XF0e(ifile),YF0e(ifile),ZF0e(ifile)
     &     ,ZPF0e(ifile),YPF0e(ifile)
     &     ,BXF0e(ifile),BYF0e(ifile),BZF0e(ifile)
     &     ,AXF0e(ifile),AYF0e(ifile),AZF0e(ifile)

         READ(LUNCOE,*)EWSe(1,ifile),EWSe(2,ifile),
     &     EWSe(3,ifile)
         READ(LUNCOE,*)EWSFe(1,ifile),EWSFe(2,ifile),
     &     EWSFe(3,ifile)
         READ(LUNCOE,*)A0SCALEe(ifile)

         if (
     &       x0e(ifile).ne.x0.or.y0e(ifile).ne.y0.or.z0e(ifile).ne.z0.or.
     &       ewse(3,ifile).ne.ews(3).or.
     &       ewse(3,ifile).ne.ews(3).or.
     &       ewsfe(1,ifile).ne.ewsf(1).or.ewsfe(2,ifile).ne.ewsf(2).or.
     &       ewsfe(3,ifile).ne.ewsf(3)
     &       ) then
           print*,"*** Error in urad_idtrmshf: Entrance or exit plane of not consistent ***"
           print*,"ifile:",ifile
         endif

         call util_skip_comment_end(luncoe,ieof)

         do iread=1,nread
           READ (LUNCOE,*) I,J,K,L,a
           ake(i+1,j+1,k+1,l+1,ifile)=a
         enddo

         WRITE(6,*)
         WRITE(6,*)'      ',IREAD,' coeffs. read from file:'
         WRITE(6,*)COEFILE
         WRITE(6,*)
         WRITE(6,*)CODETRA
         WRITE(6,*)
         WRITE(6,*)

       enddo !nfiles

       CLOSE(LUNCOE)

       call util_sort_index(nfiles,gammae,isorte)

C--- CANONICAL MOMENTUM OF REFERENCE ORBIT

C     UNIT-VECTOR EWZ=[EWS,(0,1,0)] (CROSS-PRODUCT)

       EN=1.D0/DSQRT(
     &   EWS(3)*EWS(3)
     &   +EWS(1)*EWS(1))

       EWZ(1)=-EWS(3)*EN
       EWZ(2)= 0.0d0
       EWZ(3)=+EWS(1)*EN

C     UNIT-VECTOR EWY=[EWZ,EWS]

       EWY(1)=-EWS(2)*EWZ(3)
     &   + EWS(3)*EWZ(2)
       EWY(2)=-EWS(3)*EWZ(1)
     &   + EWS(1)*EWZ(3)
       EWY(3)=-EWS(1)*EWZ(2)
     &   + EWS(2)*EWZ(1)

       EN=1.D0/DSQRT(
     &   EWSF(3)*EWSF(3)
     &   +EWSF(1)*EWSF(1))
       EWZF(1)=-EWSF(3)*EN
       EWZF(2)= 0.
       EWZF(3)=+EWSF(1)*EN

       EWYF(1)=-EWSF(2)*EWZF(3)
     &   + EWSF(3)*EWZF(2)
       EWYF(2)=-EWSF(3)*EWZF(1)
     &   + EWSF(1)*EWZF(3)
       EWYF(3)=-EWSF(1)*EWZF(2)
     &   + EWSF(2)*EWZF(1)

C--- LINEARE TRANSFERMATRIX OHNE KOPPLUNG-TERME

       do lfile=1,nfiles

         ifile=isorte(lfile)

        if (icharge.gt.0) then
          RNENN=SQRT(1.0D0 + Zp0e(ifile)**2 + Yp0e(ifile)**2)
          PXI0e(ifile)=AZ0e(ifile)/BRHOABS + zpf0e(ifile)/RNENN
          PYI0e(ifile)=AY0e(ifile)/BRHOABS + ypf0e(ifile)/RNENN
          RNENN=SQRT(1.0D0 + ZPf0e(ifile)**2 + YPf0e(ifile)**2)
          PXf0e(ifile)=AZf0e(ifile)/BRHOABS + ZPf0e(ifile)/RNENN
          PYf0e(ifile)=AYf0e(ifile)/BRHOABS + YPf0e(ifile)/RNENN
        else
          RNENN=SQRT(1.0D0 + Zp0e(ifile)**2 + Yp0e(ifile)**2)
          PXI0e(ifile)=-AZ0e(ifile)/BRHOABS + zpf0e(ifile)/RNENN
          PYI0e(ifile)=-AY0e(ifile)/BRHOABS + ypf0e(ifile)/RNENN
          RNENN=SQRT(1.0D0 + ZPf0e(ifile)**2 + YPf0e(ifile)**2)
          PXf0e(ifile)=-AZf0e(ifile)/BRHOABS + ZPf0e(ifile)/RNENN
          PYf0e(ifile)=-AYf0e(ifile)/BRHOABS + YPf0e(ifile)/RNENN
        endif

         akoeff(1:mordng,1:mordng,1:mordng,1:mordng)=
     &     ake(1:mordng,1:mordng,1:mordng,1:mordng,ifile)

         A1100=AKOEFF(2,2,1,1)
         A2000=AKOEFF(3,1,1,1)
         A0200=AKOEFF(1,3,1,1)
         A1000=AKOEFF(2,1,1,1)
         A0100=AKOEFF(1,2,1,1)

         A0011=AKOEFF(1,1,2,2)
         A0020=AKOEFF(1,1,3,1)
         A0002=AKOEFF(1,1,1,3)

         a1100e(ifile)=a1100
         a2000e(ifile)=a2000
         a0200e(ifile)=a0200
         a1000e(ifile)=a1000
         a0100e(ifile)=a0100

         a0011e(ifile)=a0011
         a0020e(ifile)=a0020
         a0002e(ifile)=a0002

         TRANSFM(1,1)=(-4.D0*AKOEFF(3,1,1,1)
     &     *AKOEFF(1,3,1,1)*AKOEFF(1,1,2,2)
     &     +2.D0*AKOEFF(3,1,1,1)*
     &     AKOEFF(1,2,2,1)*AKOEFF(1,2,1,2)
     &     +AKOEFF(2,2,1,1)**2.D0*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,2,1,1)*
     &     AKOEFF(2,1,2,1)*AKOEFF(1,2,1,2)
     &     -AKOEFF(2,2,1,1)*AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,2,1)+2.D0*
     &     AKOEFF(2,1,2,1)*AKOEFF(2,1,1,2)*AKOEFF(1,3
     &     ,1,1))/(AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,2,2)-
     &     AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(1,2)=(2.D0*AKOEFF(1,3,1,1)
     &     *AKOEFF(1,1,2,2)-AKOEFF(1,2,2,1)
     &     *AKOEFF(1,2,1,2))/(AKOEFF
     &     (2,2,1,1)*AKOEFF(1,1,2,2)-AKOEFF(2,1,1,
     &     2)*AKOEFF(1,2,2,1))
         TRANSFM(1,3)=(AKOEFF(2,2,1,1)*AKOEFF(1,2,2,1
     &     )*AKOEFF(1,1,2,2)-2.D0
     &     *AKOEFF(2,2,1,1)*AKOEFF(
     &     1,2,1,2)*AKOEFF(1,1,3,1)
     &     -2.D0*AKOEFF(2,1,2,1)*AKOEFF(1,3,1,1)
     &     *AKOEFF(1,1,2,2)+AKOEFF(2,1,2,1)
     &     *AKOEFF(1,2,2,1)*AKOEFF(1,2,1,2)
     &     +4.D0*AKOEFF(2,1,1,2)*AKOEFF(1,3,1,1)*
     &     AKOEFF(1,1,3,1)-AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,2,1)**2)/(AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,2,2)-AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,2,1))
         TRANSFM(1,4)=(AKOEFF(2,2,1,1)*AKOEFF(1,2,1,2
     &     )-2.D0*AKOEFF(2,1,1,2)
     &     *AKOEFF(1,3,1,1))/(AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,2,2)-AKOEFF(2,1,1,
     &     2)*AKOEFF(1,2,2,1))
         TRANSFM(2,1)=(-2.D0*AKOEFF(3,1,1,1)
     &     *AKOEFF(1,1,2,2)+AKOEFF(2,1,2,1)
     &     *AKOEFF(2,1,1,2))/(
     &     AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(2,2)=AKOEFF(1,1,2,2)/
     &     (AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(2,3)=(-AKOEFF(2,1,2,1)*AKOEFF(1,1,2,
     &     2)+2.D0*AKOEFF(2,1,1,2)
     &     *AKOEFF(1,1,3,1))/(
     &     AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(2,4)=(-AKOEFF(2,1,1,2))/
     &     (AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(3,1)=(4.D0*AKOEFF(3,1,1,1)
     &     *AKOEFF(1,2,2,1)*AKOEFF(1,1,1,3)
     &     -2.D0*AKOEFF(3,1,1,1)*
     &     AKOEFF(1,2,1,2)*AKOEFF(1,1,2,2)
     &     -2.D0*AKOEFF(2,2,1,1)*AKOEFF(2,1,2,1)
     &     *AKOEFF(1,1,1,3)+
     &     AKOEFF(2,2,1,1)*AKOEFF(2,1,1,2)
     &     *AKOEFF(1,1,2,2)+AKOEFF(2,1,2,1)
     &     *AKOEFF(2,1,1,2)*AKOEFF(1,2,1,2)
     &     -AKOEFF(2,1,1,2)**2.D0*AKOEFF(1,
     &     2,2,1))/(AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(3,2)=(-2.D0*AKOEFF(1,2,2,1)
     &     *AKOEFF(1,1,1,3)+AKOEFF(1,2,1,2)
     &     *AKOEFF(1,1,2,2))/(AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,2,2)-AKOEFF(2,1
     &     ,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(3,3)=(-4.D0*AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,
     &     3,1)*AKOEFF(1,1,1,3)
     &     +AKOEFF(2,2,1,1)*
     &     AKOEFF(1,1,2,2)**2+2.D0*AKOEFF(2,1,2,1)
     &     *AKOEFF(
     &     1,2,2,1)*AKOEFF(1,1,1,3)
     &     -AKOEFF(2,1,2,1)*AKOEFF(1,2,1,2)
     &     *AKOEFF(1,1,2,2)-AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,2,1)*AKOEFF(1,1,2,
     &     2)+2.D0*AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,1,2)*
     &     AKOEFF(1,1,3,1))/(AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,2,2)-AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,2,1))
         TRANSFM(3,4)=(2.D0*AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,1,3)-AKOEFF(2,1,1,2)
     &     *AKOEFF(1,2,1,2))/(AKOEFF
     &     (2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(4,1)=(2.D0*AKOEFF(3,1,1,1)
     &     *AKOEFF(1,2,2,1)-AKOEFF(2,2,1,1)
     &     *AKOEFF(2,1,2,1))/(AKOEFF
     &     (2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(4,2)=(-AKOEFF(1,2,2,1))
     &     /(AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(4,3)=(-2.D0*AKOEFF(2,2,1,1)
     &     *AKOEFF(1,1,3,1)+AKOEFF(2,1,2,1)
     &     *AKOEFF(1,2,2,1))/(
     &     AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))
         TRANSFM(4,4)=AKOEFF(2,2,1,1)/
     &     (AKOEFF(2,2,1,1)*AKOEFF(1,1,2,2)
     &     -AKOEFF(2,1,1,2)*AKOEFF(1,2,2,1))

         tfme(1:4,1:4,ifile)=transfm

         WRITE(6,*)
         WRITE(6,*)
     &     '      Linear Transfer Matrix from Generating Function'
         WRITE(6,*)
         DO IWRITE=1,4
           WRITE(6,*)'      ',(SNGL(TRANSFM(IWRITE,JWRITE))
     &       ,JWRITE=1,4)
         ENDDO
         WRITE(6,*)

       enddo !nfiles

       ICAL=1

      ENDIF   !JCAL.EQ.0

C--- INITIALIZATION }

C --- ARE WE IN ENTRANCE PLAN?

      DX=xelec-X0
      DY=yelec-Y0
      DZ=zelec-Z0

      IF (DX*EWS(1)+DY*EWS(2)+DZ*EWS(3) .GT. 1.0D-9) THEN
        WRITE(6,*)
        WRITE(6,*)
     &    '*** WARNING IN urad_idtrmshgf: STARTING POINT NOT IN ENTRANCE PLANE'
        WRITE(6,*)'xelec:',xelec
        WRITE(6,*)'yelec:',yelec
        WRITE(6,*)'zelec:',zelec
        WRITE(6,*)
      ENDIF

      YW=DY
      ZW=DZ

      V0=SQRT(vxelec**2+vyelec**2+vzelec**2)

      VxelecN=vxelec/V0
      VyelecN=vyelec/V0
      VzelecN=vzelec/V0

      SPW=VxelecN*EWS(1)+VyelecN*EWS(2)+VzelecN*EWS(3)
      YPW=VxelecN*EWY(1)+VyelecN*EWY(2)+VzelecN*EWY(3)
      ZPW=VxelecN*EWZ(1)+VyelecN*EWZ(2)+VzelecN*EWZ(3)

      YPW=YPW/SPW
      ZPW=ZPW/SPW

C--- HARDWARE APERTURE

      IF (DABS(ZW).GT.DABS(ZAPERT)
     &    .OR.  DABS(ZW+ZPW*DLAPER).GT.DABS(ZAPERT)) THEN
        IZAPER=1
        WRITE(6,*)'*** HORI. APERTURE WARNING IN urad_idtrmshgf:'
        WRITE(6,*)'ZW,ZPW:',ZW,ZPW
      ENDIF

      IF (DABS(YW).GT.DABS(YAPERT)
     &    .OR.  DABS(YW+YPW*DLAPER).GT.DABS(YAPERT)) THEN
        IYAPER=1
        WRITE(6,*)'*** VERTICAL APERTURE WARNING IN urad_idtrmshgf:'
        WRITE(6,*)'YW,YPW:',YW,YPW
      ENDIF

      IF (ilinmat.LT.0.0) THEN

C--- ONLY APPLY LINEAR TRANSFER MATRIX

        do ifile=1,nfiles
          ZWF=   tfme(1,1,ifile)*ZW + tfme(1,2,ifile)*ZPW
     &      +   tfme(1,3,ifile)*YW + tfme(1,4,ifile)*YPW
          ZPF=  tfme(2,1,ifile)*ZW + tfme(2,2,ifile)*ZPW
     &      +   tfme(2,3,ifile)*YW + tfme(2,4,ifile)*YPW
          YWF=   tfme(3,1,ifile)*ZW + tfme(3,2,ifile)*ZPW
     &      +   tfme(3,3,ifile)*YW + tfme(3,4,ifile)*YPW
          YPF=  tfme(4,1,ifile)*ZW + tfme(4,2,ifile)*ZPW
     &      +   tfme(4,3,ifile)*YW + tfme(4,4,ifile)*YPW

          XLe(ifile)=XF0e(ifile)+ZWf*EWZF(1)+YWf*EWYF(1)
          YLe(ifile)=YF0e(ifile)+ZWf*EWZF(2)+YWf*EWYF(2)
          ZLe(ifile)=ZF0e(ifile)+ZWf*EWZF(3)+YWf*EWYF(3)

          vxfe(ifile)=V0*sqrt(1.0d0-(ypf**2+zpf**2))*EWSF(1)
          vyfe(ifile)=vxfe(ifile)*YPF
          vzfe(ifile)=vxfe(ifile)*ZPF

        enddo !nfiles

      else !ilinmat .LT.0

        do ifile=1,nfiles

          IF (nfour.gt.0) THEN

            call uradbfour(xelec,yelec+vshift,zelec+hshift,bxi,byi,bzi,axi,ayi,azi)

          else IF (A0SCALE.NE.0.0D0.and.iwarna.eq.0) THEN

            print*,"*** Warning in urad_idtrmshgf: Vector potential not available ***"
            iwarna=1
c        CALL MYBFELD(xelec,yelec,zelec,BXI,BYI,BZI,AXI,AYI,AZI)

          ELSE

            AXI=0.D0
            AYI=0.D0
            AZI=0.D0

          ENDIF

          AXR=AXI*EWS(1)
     &      +AYI*EWS(2)+AZI*EWS(3)
          AYR=AXI*EWY(1)
     &      +AYI*EWY(2)+AZI*EWY(3)
          AZR=AXI*EWZ(1)
     &      +AYI*EWZ(2)+AZI*EWZ(3)

C--- KANONISCHE VARIABLEN

          AXR0=AX0e(ifile)*EWS(1)
     &      +AY0e(ifile)*EWS(2)
     &      +AZ0e(ifile)*EWS(3)

          AYR0=AX0e(ifile)*EWY(1)
     &      +AY0e(ifile)*EWY(2)
     &      +AZ0e(ifile)*EWY(3)

          AZR0=AX0e(ifile)*EWZ(1)
     &      +AY0e(ifile)*EWZ(2)
     &      +AZ0e(ifile)*EWZ(3)

          AXRF0=AXF0e(ifile)*EWSF(1)
     &      +AYF0e(ifile)*EWSF(2)
     &      +AZF0e(ifile)*EWSF(3)

          AYRF0=AXF0e(ifile)*EWYF(1)
     &      +AYF0e(ifile)*EWYF(2)
     &      +AZF0e(ifile)*EWYF(3)

          AZRF0=AXF0e(ifile)*EWZF(1)
     &      +AYF0e(ifile)*EWZF(2)
     &      +AZF0e(ifile)*EWZF(3)

          QXI= ZW
          QYI= YW
          RNENN = DSQRT(1.0D0 + ZPW**2 + YPW**2)
          PXI = (AZR-AZR0)/BRHOABS + ZPW/RNENN
          PYI = (AYR-AYR0)/BRHOABS + YPW/RNENN

C****************************************************************

C--- BERECHNE DIE SCHAETZWERTE MITTELS DER LINEAREN TRANFERMATRIX

C      XF =  TRANSFM(1,1)*X + TRANSFM(1,2)*XP
C     &   +  TRANSFM(1,3)*Z + TRANSFM(1,4)*ZP
C      XPF =  TRANSFM(2,1)*X + TRANSFM(2,2)*XP
C     &   +  TRANSFM(2,3)*Z + TRANSFM(2,4)*ZP
C      ZF =  TRANSFM(3,1)*X + TRANSFM(3,2)*XP
C     &   +  TRANSFM(3,3)*Z + TRANSFM(3,4)*ZP
C      ZPF =  TRANSFM(4,1)*X + TRANSFM(4,2)*XP
C     &   +  TRANSFM(4,3)*Z + TRANSFM(4,4)*ZP

C         RNENN = DSQRT(1.D0 + XPF**2 + ZPF**2)
C
C         PX =  XPF/RNENN
C         PY =  ZPF/RNENN
C

          PX =  TRANSFM(2,1)*QXI + TRANSFM(2,2)*PXI
     &      +  TRANSFM(2,3)*QYI + TRANSFM(2,4)*PYI
          PY =  TRANSFM(4,1)*QXI + TRANSFM(4,2)*PXI
     &      +  TRANSFM(4,3)*QYI + TRANSFM(4,4)*PYI

C****************************************************************

C      goto 321

c--  Beginn der Newton Fit Routine:

          DO JLOOP=1,JMAX

C****************************************************************

C---  POTENZTERME BERECHEN
C  Z.B. QXPOW(1)=0.,QXPOW(2)=1.,QXPOW(3)=QXI,QXPOW(4)=QXI**2

            DO IPOW=2,MORDNG
              QXPOW(IPOW+1)=QXPOW(IPOW)*QXI
              PXPOW(IPOW+1)=PXPOW(IPOW)*PX
              QYPOW(IPOW+1)=QYPOW(IPOW)*QYI
              PYPOW(IPOW+1)=PYPOW(IPOW)*PY
            END DO

            FFX=0.D0
            DFFX=0.D0
            FFY=0.D0
            DFFY=0.D0

C--- PARTIELLE ABLEITUNGEN DER ERZEUGENDEN-FUNKTION BERECHEN

            DO I=0,MORDNG-1
              DO J=0,MORDNG-1
                DO K=0,MORDNG-1
                  DO L=0,MORDNG-1

                    I1=I+1 !IN DEN FOLGENDEN BERECHNUNGEN SIND
                    !DIE I,J,K,L MATH.
                    J1=J+1 !INDIZES, DIE I1,J1,K1,L1 DIE
                    ! ENTSPRECHENDEN FORTRAN
                    K1=K+1 !INDIZES
                    L1=L+1

                    I2=I+2
                    J2=J+2
                    K2=K+2
                    L2=L+2

                    IF(I+J+K+L.GT.0 .AND. I+J+K+L.LT.MORDNG)
     &                  THEN !OHNE CLOSED ORBIT

                      FFX =
     &                  FFX + dble(I)*  AKOEFF(I1,J1,K1,L1)*
     &                  QXPOW(I2-1)*PXPOW(J2)* QYPOW(K2)*  PYPOW(L2)

                      DFFX =
     &                  DFFX + dble(I*J)*AKOEFF(I1,J1,K1,L1)*
     &                  QXPOW(I2-1)*PXPOW(J2-1)*QYPOW(K2)*PYPOW(L2)
                      FFY =
     &                  FFY  + dble(K)*  AKOEFF(I1,J1,K1,L1)*
     &                  QXPOW(I2)*  PXPOW(J2)* QYPOW(K2-1)*  PYPOW(L2)

                      DFFY =
     &                  DFFY + dble(K*L)*AKOEFF(I1,J1,K1,L1)*
     &                  QXPOW(I2)*PXPOW(J2)*QYPOW(K2-1)*PYPOW(L2-1)

                    ENDIF

                  ENDDO
                ENDDO
              ENDDO
            ENDDO

            FFX=FFX-PXI
            FFY=FFY-PYI

C--- NEUE SCHAETZWERTE BERECHEN

            DXX=0.0
            IF(DFFX.NE.0.0) DXX  = FFX/DFFX
            DYY=0.0
            IF(DFFY.NE.0.0) DYY  = FFY/DFFY
            PX   = PX - DXX
            PY   = PY - DYY

            IF((DABS(DYY)+DABS(DXX)).LT.YACC) GOTO 123

          ENDDO

123       CONTINUE

          IF (JLOOP.GE.JMAX) THEN

            WRITE(6,*)
            WRITE(6,*)
     &        '*** WARNING SR urad_idtrmshgf: NEWTOWN-FIT FAILED ***'
            WRITE(6,*)'Z,ZP,Y,YP:'
            WRITE(6,*)ZW,ZPW,YW,YPW
            WRITE(6,*)'FFX,DXX,FFY,DYY:'
            WRITE(6,*)FFX,DXX,FFY,DYY
            WRITE(6,*)'FFX,DXX,FFY,DYY:'
            WRITE(6,*)FFX,DXX,FFY,DYY
            WRITE(6,*)
            WRITE(6,*)

          ENDIF

c-- Ende der Newton Fit Routine:

321       CONTINUE

CC Hier werden die End-Koordinaten berechnet

C****************************************************************
C---  POTENZTERME BERECHEN
C  Z.B. QXPOW(1)=0.,QXPOW(2)=1.,QXPOW(3)=QXI,QXPOW(4)=QXI**2

          DO IPOW=2,MORDNG
C          QXPOW(IPOW+1)=QXPOW(IPOW)*QXI
            PXPOW(IPOW+1)=PXPOW(IPOW)*PX
C          QYPOW(IPOW+1)=QYPOW(IPOW)*QYI
            PYPOW(IPOW+1)=PYPOW(IPOW)*PY
          END DO

C--- KOORDINATEN BERECHNEN

          QX=0.0D0
          QY=0.0D0

          DO I=0,MORDNG-1
            DO J=0,MORDNG-1
              DO K=0,MORDNG-1
                DO L=0,MORDNG-1

                  I1=I+1 !IN DEN FOLGENDEN BERECHNUNGEN
                  !SIND DIE I,J,K,L MATH.
                  J1=J+1 !INDIZES, DIE I1,J1,K1,L1 DIE
                  !ENTSPRECHENDEN FORTRAN
                  K1=K+1 !INDIZES
                  L1=L+1

                  I2=I+2
                  J2=J+2
                  K2=K+2
                  L2=L+2

                  IF(I+J+K+L.GT.0 .AND. I+J+K+L.LT.MORDNG)
     &                THEN !OHNE CLOSED ORBIT

                    QX =  QX + dble(J)*  AKOEFF(I1,J1,K1,L1)*
     &                QXPOW(I2)*PXPOW(J2-1)* QYPOW(K2)*  PYPOW(L2)

                    QY = QY + dble(L)*  AKOEFF(I1,J1,K1,L1)*
     &                QXPOW(I2)*  PXPOW(J2)* QYPOW(K2)*  PYPOW(L2-1)

                  ENDIF

                ENDDO
              ENDDO
            ENDDO
          ENDDO

C****************************************************************

1234      CONTINUE

C     UMSETZTEN DER KANONISCHEN VARIABLEN IN KOORDINATEN:

          ZW  = QX
          YW  = QY

          XLe(ifile)=XF0e(ifile)+ZW*EWZF(1)+YW*EWYF(1)
          YLe(ifile)=YF0e(ifile)+ZW*EWZF(2)+YW*EWYF(2)
          ZLe(ifile)=ZF0e(ifile)+ZW*EWZF(3)+YW*EWYF(3)

          IF (nfour.gt.0) THEN

            call uradbfour(xl,yl+vshift,zl+hshift,bxf,byf,bzf,axf,ayf,azf)

          else IF (A0SCALE.NE.0.0D0.and.iwarna.eq.0) THEN

            print*,"*** Warning in urad_idtrmshgf: Vector potential not available ***"
            iwarna=1

c        CALL MYBFELD(XL,YL,ZL,BXF,BYF,BZF,AXF,AYF,AZF)

          ELSE

            AXF=0.D0
            AYF=0.D0
            AZF=0.D0

          ENDIF

          AXR=AXF*EWSF(1)
     &      +AYF*EWSF(2)+AZF*EWSF(3)
          AYR=AXF*EWYF(1)
     &      +AYF*EWYF(2)+AZF*EWYF(3)
          AZR=AXF*EWZF(1)
     &      +AYF*EWZF(2)+AZF*EWZF(3)

          PAX=PX-(AZR-AZRF0)/BRHOABS
          PAY=PY-(AYR-AYRF0)/BRHOABS

          SNENN2 = 1.0D0 - PAX*PAX - PAY*PAY

c spezielle Einschub falls SNENN2<0 wird. Dann soll diese Subroutine
c nicht hier aussteigen sondern irgenwoanders im BETA-CODE. Dazu wird
c XPF und ZPF mit Faktor 1.D6 multipliziert:

          IF ( SNENN2 .GT. 0.0) THEN

            SNENN = DSQRT(SNENN2)

            ZPW = PAX/SNENN
            YPW = PAY/SNENN

          ELSE

            WRITE(6,*)'*** WARNING IN urad_idtrmshgf:'
            WRITE(6,*)
     &        '*** NEGATIVE ROOT OF CANONICAL MOMENTUM -> INSTABILITY'


            ZPW = PX*1.0D6
            YPW = PY*1.0D6
            istatus=-11

          ENDIF

          vxfe(ifile)=V0*sqrt(1.0d0-(ypw**2+zpw**2))*EWSF(1)
          vyfe(ifile)=vxfe(ifile)*YPw
          vzfe(ifile)=vxfe(ifile)*ZPw

      enddo !ifile=1,nfiles

      endif !ilinmat .LT.0

      if (nfiles.eq.1) then
        if (abs(gammai-gammae(1)).gt.1.0d-10) then
          print*,"*** Error in urad_idtrmshgf: Gamma out of range ***"
          istatus=-1
          return
        endif
        zwf=zwfe(1)
        zpf=zpfe(1)
        ywf=ywfe(1)
        ypf=ypfe(1)
        xexit=XF0e(1)+ZWF*EWZF(1)+YWF*EWYF(1)
        yexit=YF0+ZWF*EWZF(2)+YWF*EWYF(2)
        zexit=ZF0+ZWF*EWZF(3)+YWF*EWYF(3)
        vxexit=V0*EWSF(1)
        vyexit=vxexit*YPF
        vzexit=vxexit*ZPF
      else
        mode=0
        call util_interpol_parabel(nfiles,gammae,xle,
     &    gammai,xexit,p,pp,mode,ifail)
        if (ifail.ne.0) then
          print*,"*** Error in util_interpol_parabel ***"
          istatus=-2
          return
        endif
        mode=0
        call util_interpol_parabel(nfiles,gammae,yle,
     &    gammai,yexit,p,pp,mode,ifail)
        if (ifail.ne.0) then
          print*,"*** Error in util_interpol_parabel ***"
          istatus=-2
          return
        endif
        mode=0
        call util_interpol_parabel(nfiles,gammae,zle,
     &    gammai,zexit,p,pp,mode,ifail)
        if (ifail.ne.0) then
          print*,"*** Error in util_interpol_parabel ***"
          istatus=-2
          return
        endif
        call util_interpol_parabel(nfiles,gammae,vxfe,
     &    gammai,vxexit,p,pp,mode,ifail)
        if (ifail.ne.0) then
          print*,"*** Error in util_interpol_parabel ***"
          istatus=-2
          return
        endif
        mode=0
        call util_interpol_parabel(nfiles,gammae,vyfe,
     &    gammai,vyexit,p,pp,mode,ifail)
        if (ifail.ne.0) then
          print*,"*** Error in util_interpol_parabel ***"
          istatus=-2
          return
        endif
        mode=0
        call util_interpol_parabel(nfiles,gammae,vzfe,
     &    gammai,vzexit,p,pp,mode,ifail)
        if (ifail.ne.0) then
          print*,"*** Error in util_interpol_parabel ***"
          istatus=-2
          return
        endif
      endif

      RETURN
      END
