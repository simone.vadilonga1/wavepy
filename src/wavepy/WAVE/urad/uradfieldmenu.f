*CMZ :  4.00/13 16/11/2021  16.18.56  by  Michael Scheer
*CMZ :  3.05/05 09/07/2018  15.22.23  by  Michael Scheer
*CMZ :  3.05/04 05/07/2018  08.56.42  by  Michael Scheer
*CMZ :  3.04/00 23/01/2018  17.17.28  by  Michael Scheer
*CMZ :  3.03/04 04/12/2017  15.56.53  by  Michael Scheer
*CMZ :  3.03/02 18/11/2015  12.56.22  by  Michael Scheer
*CMZ :  3.02/04 13/03/2015  10.36.11  by  Michael Scheer
*CMZ :  2.70/00 12/11/2012  11.53.09  by  Michael Scheer
*CMZ :  2.68/04 04/09/2012  09.38.42  by  Michael Scheer
*CMZ :  2.68/03 29/08/2012  12.25.27  by  Michael Scheer
*-- Author :    Michael Scheer   22/08/2012
      subroutine uradfieldmenu(x,y,z,bxout,byout,bzout,ex,ey,ez,gamma,istatus)

c Author: Michael Scheer, Michael.Scheer@Helmholtz-Berlin.de

c NO WARRANTY

*KEEP,gplhint.
!******************************************************************************
!
!      Copyright 2013 Helmholtz-Zentrum Berlin (HZB)
!      Hahn-Meitner-Platz 1
!      D-14109 Berlin
!      Germany
!
!      Author Michael Scheer, Michael.Scheer@Helmholtz-Berlin.de
!
! -----------------------------------------------------------------------
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy (wave_gpl.txt) of the GNU General Public
!    License along with this program.
!    If not, see <http://www.gnu.org/licenses/>.
!
!    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
!    der GNU General Public License, wie von der Free Software Foundation,
!    Version 3 der Lizenz oder (nach Ihrer Option) jeder spaeteren
!    veroeffentlichten Version, weiterverbreiten und/oder modifizieren.
!
!    Dieses Programm wird in der Hoffnung, dass es nuetzlich sein wird, aber
!    OHNE JEDE GEWAEHRLEISTUNG, bereitgestellt; sogar ohne die implizite
!    Gewaehrleistung der MARKTFAEHIGKEIT oder EIGNUNG FueR EINEN BESTIMMTEN ZWECK.
!    Siehe die GNU General Public License fuer weitere Details.
!
!    Sie sollten eine Kopie (wave_gpl.txt) der GNU General Public License
!    zusammen mit diesem Programm erhalten haben. Wenn nicht,
!    siehe <http://www.gnu.org/licenses/>.
!
!******************************************************************************
*KEND.

      implicit none

*KEEP,phyconparam.
      include 'phyconparam.cmn'
*KEEP,uradcom.
      integer nfour,nfourwls,ifour0,iprntf,maxfoumagp,nfoumags,nfoumagcp
      parameter (maxfoumagp=100,nfoumagcp=2**10)

      double precision
     &  xfoubounds(5,maxfoumagp),foumags(nfoumagcp/2+3,maxfoumagp)
     &  ,fouentr,fouexit,xshbfour

c      character(2048) chfoumags(maxfoumagp)

      integer kmonopole,intpolbmap,kbmap,kmagseq,kbmapu,kmagsequ

      double precision xlenfour,xbhomf,dbhomf,emom
      character(2048) fmagseq

      common/cfourier/
     &  xlenfour,xbhomf,dbhomf,emom
     &  ,xfoubounds,foumags
     &  ,fouentr,fouexit,xshbfour
     &  ,kmagseq,fmagseq
     &  ,nfour,nfourwls,ifour0,iprntf,nfoumags,kmonopole

      integer iahwfour,nhhalbasy

      double precision b0scale,hshift,vshift

      integer khalbasy

      common/bscalec/ b0scale,hshift,vshift,intpolbmap

      double precision b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur

      common/uradhalbasym/
     &  b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur,
     &  khalbasy,iahwfour

      integer nmgsqp,mmag,irfilf,iwfilf
      parameter(nmgsqp=10000)

      character(3) ctyp(nmgsqp)

      double precision pmag(13,nmgsqp),coz(2,nmgsqp+1)
     &  ,corr(nmgsqp),uebounds(2,nmgsqp),
     &  dibounds(2,nmgsqp),
     &  dhbounds(2,nmgsqp),
     &  qfbounds(2,nmgsqp),
     &  sxbounds(2,nmgsqp),
     &  bmsqbounds(2)

      common/uradmgsqc/pmag,coz,corr,dibounds,dhbounds,
     &  uebounds,qfbounds,sxbounds,bmsqbounds,mmag,irfilf,iwfilf,
     &  kbmapu,kmagsequ,
     &  ctyp

      namelist/ufield/nfour,nfourwls,ifour0,xlenfour,dbhomf,iprntf,irfilf,
     &  b0halbasy,xlhalbasy,ylhalbasy,hhalbasy,pkhalbasy,
     &  zlhalbasy,fasym,ahwpol,iahwfour,xcenhal,nhhalbasy,
     &  b0scale,hshift,vshift,kmonopole,intpolbmap,kbmap,kmagseq,fmagseq
*KEND.

      double precision, dimension (:,:), allocatable :: qgrid

      double precision x,y,z,bx,by,bz,ex,ey,ez,tolerance,
     &  ax,ay,az,bxout,byout,bzout,gamma

      integer istatus,lun,interpol,ifail,ical,nq,ieof,lunm
      integer :: kstatus=0, iwarnx=0,iwarny=0,iwarnz=0

      data lun/20/
      data ical/0/
      data tolerance/0.001d0/

      save ical,lun,tolerance,iwarnx,iwarny,iwarnz

      ugamma=gamma
      uenergy=gamma*emassg1
      emom=emassg1*dsqrt((gamma-1.0d0)*(gamma+1.0d0)) !GeV

      interpol=intpolbmap

      bxout=0.0d0
      byout=0.0d0
      bzout=0.0d0

      if (kmonopole.ne.0) then
        if (ical.eq.0) then
          open(newunit=lunm,file="util_b_monopoly.fit")
          ieof=0
          nq=0
          do while (ieof==0)
            call util_skip_comment_end(lunm,ieof)
            if (ieof.ne.0) exit
            read(lunm,*)bx
            nq=nq+1
          enddo
          allocate(qgrid(4,nq))
          rewind(lunm)
          ieof=0
          nq=0
          do while (ieof==0)
            call util_skip_comment_end(lunm,ieof)
            if (ieof.ne.0) exit
            nq=nq+1
            read(lunm,*)qgrid(1:4,nq)
          enddo
          close(lunm)
        endif
        call util_b_monopoly(nq,qgrid,x,y+vshift,z+hshift,bx,by,bz,istatus)
        bxout=bxout+bx
        byout=byout+by
        bzout=bzout+bz
      endif !kmonopole

      if (nfour.ne.0) then
        call uradbfour(x,y+vshift,z+hshift,bx,by,bz,ax,ay,az)
        bxout=bxout+bx
        byout=byout+by
        bzout=bzout+bz
      endif

      if (kmagsequ.ne.0) then
        call uradbmagseq(x,y+vshift,z+hshift,bx,by,bz,ax,ay,az)
        bxout=bxout+bx
        byout=byout+by
        bzout=bzout+bz
      endif

      if (kbmapu.ne.0) then
        call uradbmap(lun,x,y+vshift,z+hshift,bx,by,bz,tolerance,
     &    interpol,istatus)
        if (istatus.eq.1001) then
          if (iwarnx.eq.0) then
            print*,"*** Warning in uradfieldmenu: x out of bmap, x=",x
            print*,"*** Further warning will be suppressed for this error ***"
            iwarnx=1
          endif
          istatus=0
        endif
        if (istatus.eq.1002) then
          if (iwarny.eq.0) then
            print*,"*** Warning in uradfieldmenu: y out of bmap, y=",y+vshift
            print*,"*** Further warning will be suppressed for this error ***"
            iwarny=1
          endif
          istatus=0
        endif
        if (istatus.eq.1003) then
          if (iwarnz.eq.0) then
            print*,"*** Warning in uradfieldmenu: z out of bmap, z=",z+hshift
            print*,"*** Further warning will be suppressed for this error ***"
            iwarnz=1
          endif
          istatus=0
        endif
        bxout=bxout+bx
        byout=byout+by
        bzout=bzout+bz
      endif

      if (istatus.ne.0) then
        kstatus=kstatus+istatus
      endif

      istatus=kstatus

      bx=bx*b0scale
      by=by*b0scale
      bz=bz*b0scale

      ex=0.0d0
      ey=0.0d0
      ez=0.0d0

      if (ical.eq.0) ical=1

      return
      end
