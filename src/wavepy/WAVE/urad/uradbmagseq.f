*CMZ :  4.00/13 28/10/2021  13.59.10  by  Michael Scheer
*CMZ :  3.05/04 02/07/2018  17.35.45  by  Michael Scheer
*CMZ :  3.04/00 24/01/2018  13.24.26  by  Michael Scheer
*CMZ :  3.03/04 11/10/2017  11.26.09  by  Michael Scheer
*CMZ :  3.03/02 18/03/2016  14.37.19  by  Michael Scheer
*CMZ :  3.02/00 28/08/2014  08.47.45  by  Michael Scheer
*CMZ :  3.01/01 29/07/2013  14.03.24  by  Michael Scheer
*CMZ :  3.01/00 17/07/2013  16.10.24  by  Michael Scheer
*CMZ :  3.00/01 28/03/2013  12.44.42  by  Michael Scheer
*CMZ :  2.70/12 01/03/2013  16.28.23  by  Michael Scheer
*CMZ :  2.68/03 07/08/2012  11.36.24  by  Michael Scheer
*CMZ :  2.68/02 06/07/2012  13.23.04  by  Michael Scheer
*CMZ :  2.67/06 24/05/2012  12.31.33  by  Michael Scheer
*CMZ :  2.66/20 06/07/2011  09.41.56  by  Michael Scheer
*CMZ :  2.66/07 12/05/2010  13.34.28  by  Michael Scheer
*CMZ :  2.63/05 23/10/2009  09.19.41  by  Michael Scheer
*CMZ :  2.61/02 15/03/2007  11.13.54  by  Michael Scheer
*CMZ :  2.53/01 24/01/2005  10.56.03  by  Michael Scheer
*CMZ :  2.52/11 06/12/2004  15.54.00  by  Michael Scheer
*CMZ :  2.47/10 30/05/2003  11.44.20  by  Michael Scheer
*CMZ :  2.41/10 14/08/2002  17.34.01  by  Michael Scheer
*CMZ :  2.20/03 23/02/2001  11.01.50  by  Michael Scheer
*CMZ :  2.15/00 28/04/2000  10.32.33  by  Michael Scheer
*CMZ :  1.02/03 07/01/98  11.52.22  by  Michael Scheer
*CMZ :  1.02/00 06/01/98  15.08.07  by  Michael Scheer
*CMZ :  1.01/00 28/10/97  12.14.09  by  Michael Scheer
*CMZ : 00.01/08 01/04/95  16.54.24  by  Michael Scheer
*CMZ : 00.01/07 10/03/95  11.22.55  by  Michael Scheer
*CMZ : 00.01/02 04/11/94  15.21.20  by  Michael Scheer
*CMZ : 00.00/04 29/04/94  17.48.03  by  Michael Scheer
*CMZ : 00.00/00 28/04/94  16.13.42  by  Michael Scheer
*-- Author : Michael Scheer
      SUBROUTINE uradbmagseq(xin,yin,zin,bxout,byout,bzout,axout,ayout,azout)
*KEEP,gplhint.
!******************************************************************************
!
!      Copyright 2013 Helmholtz-Zentrum Berlin (HZB)
!      Hahn-Meitner-Platz 1
!      D-14109 Berlin
!      Germany
!
!      Author Michael Scheer, Michael.Scheer@Helmholtz-Berlin.de
!
! -----------------------------------------------------------------------
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy (wave_gpl.txt) of the GNU General Public
!    License along with this program.
!    If not, see <http://www.gnu.org/licenses/>.
!
!    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
!    der GNU General Public License, wie von der Free Software Foundation,
!    Version 3 der Lizenz oder (nach Ihrer Option) jeder spaeteren
!    veroeffentlichten Version, weiterverbreiten und/oder modifizieren.
!
!    Dieses Programm wird in der Hoffnung, dass es nuetzlich sein wird, aber
!    OHNE JEDE GEWAEHRLEISTUNG, bereitgestellt; sogar ohne die implizite
!    Gewaehrleistung der MARKTFAEHIGKEIT oder EIGNUNG FueR EINEN BESTIMMTEN ZWECK.
!    Siehe die GNU General Public License fuer weitere Details.
!
!    Sie sollten eine Kopie (wave_gpl.txt) der GNU General Public License
!    zusammen mit diesem Programm erhalten haben. Wenn nicht,
!    siehe <http://www.gnu.org/licenses/>.
!
!******************************************************************************
*KEND.

C--- READ DATA-FILE BMAGSEQ.IN, THAT CONTAINS MAGNET CONFIGURATION
C    STRUCTURE IS CENTERED AROUND ORIGIN

      IMPLICIT NONE


      INTEGER ICAL,IM,ieof,imag,lunmg

*KEEP,phycon.
      include 'phycon.cmn'
*KEEP,uradcom.
      integer nfour,nfourwls,ifour0,iprntf,maxfoumagp,nfoumags,nfoumagcp
      parameter (maxfoumagp=100,nfoumagcp=2**10)

      double precision
     &  xfoubounds(5,maxfoumagp),foumags(nfoumagcp/2+3,maxfoumagp)
     &  ,fouentr,fouexit,xshbfour

c      character(2048) chfoumags(maxfoumagp)

      integer kmonopole,intpolbmap,kbmap,kmagseq,kbmapu,kmagsequ

      double precision xlenfour,xbhomf,dbhomf,emom
      character(2048) fmagseq

      common/cfourier/
     &  xlenfour,xbhomf,dbhomf,emom
     &  ,xfoubounds,foumags
     &  ,fouentr,fouexit,xshbfour
     &  ,kmagseq,fmagseq
     &  ,nfour,nfourwls,ifour0,iprntf,nfoumags,kmonopole

      integer iahwfour,nhhalbasy

      double precision b0scale,hshift,vshift

      integer khalbasy

      common/bscalec/ b0scale,hshift,vshift,intpolbmap

      double precision b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur

      common/uradhalbasym/
     &  b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur,
     &  khalbasy,iahwfour

      integer nmgsqp,mmag,irfilf,iwfilf
      parameter(nmgsqp=10000)

      character(3) ctyp(nmgsqp)

      double precision pmag(13,nmgsqp),coz(2,nmgsqp+1)
     &  ,corr(nmgsqp),uebounds(2,nmgsqp),
     &  dibounds(2,nmgsqp),
     &  dhbounds(2,nmgsqp),
     &  qfbounds(2,nmgsqp),
     &  sxbounds(2,nmgsqp),
     &  bmsqbounds(2)

      common/uradmgsqc/pmag,coz,corr,dibounds,dhbounds,
     &  uebounds,qfbounds,sxbounds,bmsqbounds,mmag,irfilf,iwfilf,
     &  kbmapu,kmagsequ,
     &  ctyp

      namelist/ufield/nfour,nfourwls,ifour0,xlenfour,dbhomf,iprntf,irfilf,
     &  b0halbasy,xlhalbasy,ylhalbasy,hhalbasy,pkhalbasy,
     &  zlhalbasy,fasym,ahwpol,iahwfour,xcenhal,nhhalbasy,
     &  b0scale,hshift,vshift,kmonopole,intpolbmap,kbmap,kmagseq,fmagseq
*KEEP,URADOMP.
      integer ithread,kelec
      common/uradompc/ithread,kelec
*KEND.

      DOUBLE PRECISION BXOUT,BYOUT,BZOUT,AXOUT,AYOUT,AZOUT
      DOUBLE PRECISION XIN,YIN,ZIN
      double precision shift,xcen,perlen,pern,xlamb,totlen

      DOUBLE PRECISION
     &  xlen2,dint,bx,by,bz,xfour(nfoumagcp+2+2),dxfour

      COMPLEX CKOEF(nfoumagcp/2+1+2)
      REAL*4  YFOUR(nfoumagcp+2+2)
      EQUIVALENCE (CKOEF,YFOUR)

      integer ip,i1,i,mfour,k

      CHARACTER(3) CDUM2
      CHARACTER(5) CDUM1

      DATA ICAL/0/

      save ical

C--- INITIALISATION

c      print*,"bmagseq 1:",ithread,kelec,ical

      IF (ICAL.EQ.0) THEN

C- OPEN FILE, READ FIRST TIME IN ORDER TO DECODE MAGNET-TYPES

        OPEN(newUNIT=LUNMG,FILE=fmagseq,FORM='FORMATTED',STATUS='OLD')

        DO IM=1,NMGSQP
          call util_skip_comment_end(lunmg,ieof)
          if (ieof.ne.0) goto 99
          READ(LUNMG,*,END=99) CDUM1,CTYP(IM)
        ENDDO !IM

99      CONTINUE

        mmag=IM-1

C- REWIND FILE AND READ AGAIN TO GET PARAMETERS

        REWIND(LUNMG)

        nfoumags=0

        DO IM=1,mmag

          call util_skip_comment_end(lunmg,ieof)
          CORR(IM)=1.0D0

          IF     (CTYP(IM).EQ.'DI') THEN
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1,IM),PMAG(2,IM),PMAG(3,IM),PMAG(4,IM)
          else IF (CTYP(IM).EQ.'DH') THEN
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1,IM),PMAG(2,IM),PMAG(3,IM),PMAG(4,IM)
          else IF (CTYP(IM).EQ.'DIF') THEN
            nfoumags=nfoumags+1
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1,IM),PMAG(2,IM),PMAG(3,IM),PMAG(4,IM),
     &        pmag(5,im),pmag(6,im)
            xfoubounds(4,nfoumags)=pmag(5,im)
            xfoubounds(5,nfoumags)=pmag(6,im)
            if (pmag(6,im).gt.nfoumagcp) then
              print*,"*** Error in URADBMAGSEQ: Number of Fourier coefficients exceeds dimension nfoumagcp =",nfoumagcp,"  ***"
              stop "*** Program WAVE aborted ***"
            endif
          else IF (CTYP(IM).EQ.'DHF') THEN
            nfoumags=nfoumags+1
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1,IM),PMAG(2,IM),PMAG(3,IM),PMAG(4,IM),
     &        pmag(5,im),pmag(6,im)
            xfoubounds(4,nfoumags)=pmag(5,im)
            xfoubounds(5,nfoumags)=pmag(6,im)
            if (pmag(6,im).gt.nfoumagcp) then
              print*,"*** Error in URADBMAGSEQ: Number of Fourier coefficients exceeds dimension nfoumagcp =",nfoumagcp,"  ***"
              stop "*** Program WAVE aborted ***"
            endif
          ELSE IF (CTYP(IM).EQ.'QP'.OR.CTYP(IM).EQ.'QF') THEN
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1,IM),PMAG(2,IM),PMAG(3,IM),PMAG(4,IM),PMAG(5,IM)
          ELSE IF (CTYP(IM).EQ.'SX') THEN
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1,IM),PMAG(2,IM),PMAG(3,IM),PMAG(4,IM),PMAG(5,IM)
          ELSE IF (CTYP(IM).EQ.'UE') THEN
            READ(LUNMG,*)CDUM1,CDUM2,
     &        PMAG(1:13,IM)
            !1. 2.   3.     4.    5.      6.     7.     8.       9.   10.   11.
            !K  B0V, B0H, Shift, XCen, PerLen, NPer, Lambda_X, Nharm Eharm ctaper ang
            shift=pmag(4,im)
            xcen=pmag(5,im)
            perlen=pmag(6,im)
            pern=pmag(7,im)
            xlamb=pmag(8,im)
            ahwpol=((pern-1.0d0)*2+1.0d0)
            totlen=perlen*((ahwpol-1.0d0)/2.0d0+1.0d0)+shift
            uebounds(1,im)=xcen-0.5d0*totlen
            uebounds(2,im)=xcen+0.5d0*totlen
          ELSE

            WRITE(6,*)
            WRITE(6,*)'*** ERROR IN URADBMAGSEQ ***'
            WRITE(6,*)'ILLEGAL MAGNET TYP ',ctyp(im), ' ON FILE ',
     &        trim(fmagseq)
            WRITE(6,*)

            STOP

          ENDIF !CTYP

        ENDDO   !IM

        CLOSE(LUNMG)

        if (nfoumags.gt.maxfoumagp) then
          print*
          print*,"*** Error in URADBMAGSEQ: Too many Fourier magnets ***"
          print*,"*** Please, increase MAXFOUMAGSP and recompile WAVE"
          print*,"*** Program WAVE aborted ***"
          print*
          stop
        endif

C---{ CORRECT FOR FRINGE-FIELD-EFFECTS

        DO IM=1,mmag
          IF (CTYP(IM).EQ.'DI'.or.ctyp(im).eq.'DIF'.or.
     &        CTYP(IM).EQ.'DH'.or.ctyp(im).eq.'DHF') THEN
            xlen2=dabs(pmag(2,im)*sin(pmag(1,im)/2.0d0))
            dint=exp(2.0d0*pmag(4,im)*xlen2)/
     &        (pmag(4,im)*(exp(2.0d0*pmag(4,im)*xlen2)-1.0d0))*
     &        2.0d0*pmag(4,im)*xlen2
            corr(im)=corr(im)/dint*(2.0d0*xlen2)
          ENDIF !DI
        ENDDO  !mmag

c        if (nfoumags.gt.0) then

        mfour=nint(alog(float(nfoumagcp))/alog(2.0E0))
        nfoumags=0

        DO imag=1,mmag

          IF (CTYP(IMag).EQ.'DIF') THEN
            nfoumags=nfoumags+1
            xlen2=dabs(pmag(2,imag)*sin(pmag(1,imag)/2.0d0))
     &        +70.0d0/pmag(4,imag)
            dxfour=4.0d0*xlen2/nfoumagcp
            do i=1,nfoumagcp/2+1
              xfour(i)=-dxfour*(nfoumagcp/2+1-i)
              xfour(nfoumagcp+1-i+1)=-xfour(i)
            enddo
            do i=1,nfoumagcp/2+1
              i1=i-1
              ip=nfoumagcp/2+1+i1
              im=nfoumagcp/2+1-i1
              call uradbdi(pmag(3,imag)-xfour(ip),0.0d0,0.0d0,bx,by,bz,imag)
              yfour(ip)=sngl(by)
              yfour(im)=sngl(by)
            enddo
            xfoubounds(1,nfoumags)=imag
            xfoubounds(2,nfoumags)=xfour(1)+pmag(3,imag)
            xfoubounds(3,nfoumags)=-xfour(1)+pmag(3,imag)
            call rfft(ckoef,-mfour) !fft mit cern-routine d703
            do k=1,nint(xfoubounds(5,nfoumags))  !reelle koeffizienten
              foumags(k,nfoumags)=(-1.)**(k-1)*2.0*real(ckoef(k))
            enddo
          else IF (CTYP(IMag).EQ.'DHF') THEN
            nfoumags=nfoumags+1
            xlen2=dabs(pmag(2,imag)*sin(pmag(1,imag)/2.0d0))
     &        +70.0d0/pmag(4,imag)
            dxfour=4.0d0*xlen2/nfoumagcp
            do i=1,nfoumagcp/2+1
              xfour(i)=-dxfour*(nfoumagcp/2+1-i)
              xfour(nfoumagcp+1-i+1)=-xfour(i)
            enddo
            do i=1,nfoumagcp/2+1
              i1=i-1
              ip=nfoumagcp/2+1+i1
              im=nfoumagcp/2+1-i1
              call uradbdh(pmag(3,imag)-xfour(ip),0.0d0,0.0d0,bx,by,bz,imag)
              yfour(ip)=sngl(bz)
              yfour(im)=sngl(bz)
            enddo
            xfoubounds(1,nfoumags)=imag
            xfoubounds(2,nfoumags)=xfour(1)+pmag(3,imag)
            xfoubounds(3,nfoumags)=-xfour(1)+pmag(3,imag)
            call rfft(ckoef,-mfour) !fft mit cern-routine d703
            do k=1,nint(xfoubounds(5,nfoumags))  !reelle koeffizienten
              foumags(k,nfoumags)=(-1.)**(k-1)*2.0*real(ckoef(k))
            enddo
          ENDIF !CTYP

        ENDDO   !IM

c        endif !nfoumags

      ENDIF !ICAL

      if (ical.lt.10) ical=ical+1

      if (ical.eq.2) then
        WRITE(6,*)
        WRITE(6,*)'     SR URADBMAGSEQ: Magnets read from file:'
        WRITE(6,*)'     ',fmagseq(1:len_trim(fmagseq))
        WRITE(6,1100)mmag,NMGSQP
1100    FORMAT('      Number of magnets: ',I6,' (Limit IS ',I6,' Magnets)')
        WRITE(6,*)
        WRITE(6,*)'     Typ Boundaries Parameters'
        WRITE(6,*)
        DO IM=1,mmag
          if (ctyp(im).eq.'DI') then
            WRITE(6,1201) CTYP(IM),dibounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'QF') then
            WRITE(6,1201) CTYP(IM),qfbounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'QP') then
            WRITE(6,1201) CTYP(IM),qfbounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'SX') then
            WRITE(6,1201) CTYP(IM),sxbounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'DH') then
            WRITE(6,1201) CTYP(IM),dhbounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'DIF') then
            WRITE(6,1201) CTYP(IM),dibounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'DHF') then
            WRITE(6,1201) CTYP(IM),dhbounds(1:2,im),PMAG(1:6,IM)
          else if (ctyp(im).eq.'UE') then
            WRITE(6,1201) CTYP(IM),uebounds(1:2,im),PMAG(1:13,IM)
          endif
1201      FORMAT('      ',A3,15E14.6)
        enddo
        write(6,*)
        WRITE(6,*)
        ICAL=2
      ENDIF !ICAL

C--- MAGNETIC FIELD

      call uradBMAGSEQC(XIN,YIN,ZIN,BXOUT,BYOUT,BZOUT,AXOUT,AYOUT,AZOUT)

c      print*,"bmagseq 2:",ithread,kelec,ical
      RETURN
      END
c      include 'uradbsx.f'
c      include 'uradbdh.f'
c      include 'uradbdi.f'
c      include 'uradbqp.f'
c      include 'uradbqf.f'
c      include 'uradrfft.f'
c      include 'uradcfft.f'
