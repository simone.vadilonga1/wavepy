*CMZ :  3.05/04 28/06/2018  08.46.17  by  Michael Scheer
*CMZ :  3.04/00 17/01/2018  15.15.09  by  Michael Scheer
*CMZ :  3.03/04 04/12/2017  14.30.13  by  Michael Scheer
*CMZ :  3.01/00 16/07/2013  09.32.23  by  Michael Scheer
*-- Author : Michael Scheer
      subroutine uradbfour(xin,yin,zin,bxout,byout,bzout,axout,ayout,azout)

c subroutine calculates magnetic field expansion of halbach wiggler.
c fourier coefficients are read from data file

      implicit none

*KEEP,uradcom.
      integer nfour,nfourwls,ifour0,iprntf,maxfoumagp,nfoumags,nfoumagcp
      parameter (maxfoumagp=100,nfoumagcp=2**10)

      double precision
     &  xfoubounds(5,maxfoumagp),foumags(nfoumagcp/2+3,maxfoumagp)
     &  ,fouentr,fouexit,xshbfour

c      character(2048) chfoumags(maxfoumagp)

      integer kmonopole,intpolbmap,kbmap,kmagseq,kbmapu,kmagsequ

      double precision xlenfour,xbhomf,dbhomf,emom
      character(2048) fmagseq

      common/cfourier/
     &  xlenfour,xbhomf,dbhomf,emom
     &  ,xfoubounds,foumags
     &  ,fouentr,fouexit,xshbfour
     &  ,kmagseq,fmagseq
     &  ,nfour,nfourwls,ifour0,iprntf,nfoumags,kmonopole

      integer iahwfour,nhhalbasy

      double precision b0scale,hshift,vshift

      integer khalbasy

      common/bscalec/ b0scale,hshift,vshift,intpolbmap

      double precision b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur

      common/uradhalbasym/
     &  b0halbasy,
     &  xlhalbasy,ylhalbasy,zlhalbasy,
     &  xkhalbasy,ykhalbasy,zkhalbasy,
     &  fasym,ahwpol,rhalbasy,xcenhal,hhalbasy,pkhalbasy,
     &  ugamma,uenergy,ucur,
     &  khalbasy,iahwfour

      integer nmgsqp,mmag,irfilf,iwfilf
      parameter(nmgsqp=10000)

      character(3) ctyp(nmgsqp)

      double precision pmag(13,nmgsqp),coz(2,nmgsqp+1)
     &  ,corr(nmgsqp),uebounds(2,nmgsqp),
     &  dibounds(2,nmgsqp),
     &  dhbounds(2,nmgsqp),
     &  qfbounds(2,nmgsqp),
     &  sxbounds(2,nmgsqp),
     &  bmsqbounds(2)

      common/uradmgsqc/pmag,coz,corr,dibounds,dhbounds,
     &  uebounds,qfbounds,sxbounds,bmsqbounds,mmag,irfilf,iwfilf,
     &  kbmapu,kmagsequ,
     &  ctyp

      namelist/ufield/nfour,nfourwls,ifour0,xlenfour,dbhomf,iprntf,irfilf,
     &  b0halbasy,xlhalbasy,ylhalbasy,hhalbasy,pkhalbasy,
     &  zlhalbasy,fasym,ahwpol,iahwfour,xcenhal,nhhalbasy,
     &  b0scale,hshift,vshift,kmonopole,intpolbmap,kbmap,kmagseq,fmagseq
*KEEP,phycon.
      include 'phycon.cmn'
*KEND.

      integer maxfour
      parameter (maxfour=2048)

      integer i,ik,ical,k,nkoef,icodef,ifound

      double precision a(maxfour),xkfour(maxfour),ykfour(maxfour),
     &  zkfour(maxfour),a0

      real*4 ar(maxfour),sumck2,sumc

      double precision zrfour,xl0four,yl0four,zl0four,xk0four,yk0four,zk0four,
     &  zl0four2,dumz,dumn

      double precision xin,yin,zin,bxout,byout,bzout,axout,ayout,azout,
     &  dsnxkx,dcsxkx,dshyky,dchyky,dsnzkz,dcszkz
     &  ,bxh,byh,bzh,axh,ayh,azh,an,am,x

      double precision expomy,dexpomy,expomy1

      double precision dnull

      complex*16 cdexpomx,cexpomz,cdexpomz

      integer lunf

      character(128) codef

      data dnull/0.0d0/
      data ical/0/

      save

      if (ical.ne.1) then

        call util_phycon

        open(newunit=lunf,file="urad.fou",status='old',form='formatted')

        read(lunf,'(a)')codef
        read(lunf,*)zrfour
        read(lunf,*)nkoef
        if (nfour.eq.-9999) nfour=nkoef

        if (nkoef.gt.maxfour.or.nkoef.lt.nfour) then
          write(6,*)
          write(6,*)
     &      '*** Error in uradbfour ***'
          write(6,*)'nkoef.gt.maxfour.or.nkoef.lt.nfour'
          write(6,*)'Check parameters in file cmpara.cmn and namelists fourier (and maybe halbasy)'
          write(6,*)
          stop
        endif

        read(lunf,*)ik,a0
        if (ifour0.ne.0) a0=0.0d0

        do i=1,nkoef-1
          read(lunf,*)ik,ar(ik-1)
          a(ik-1)=dble(ar(ik-1))
        end do

        zl0four=zrfour
        zl0four2=zl0four/2.
        zk0four=2.0d0*pi1/zl0four
        xl0four=xlenfour
        xk0four=0.0d0

        if(xl0four.ne.0.) xk0four=2.0d0*pi1/xl0four

        yk0four=dsqrt(zk0four**2+xk0four**2)
        yl0four=2.0d0*pi1/yk0four

        close(lunf)

        do i=1,nfour-1
          zkfour(i)=zk0four*i
          xkfour(i)=xk0four !vorerst, siehe auch oben
          ykfour(i)=dsqrt(zkfour(i)**2+xkfour(i)**2)
        end do

        ical=1

      endif

      x=dmod(xin,zl0four) !2.12.91

      if (x.gt.zl0four2) then
        x=x-zl0four
      else if (x.lt.-zl0four2) then
        x=x+zl0four
      endif

      bxh=0.
      byh=a0/2.0d0
      bzh=0.

C IF CHANGED, CONSIDER FOLLOWING LOOP AND SR MYBFELD {



      AXH= A0/2.D0*  XIN !XIN IS HERE Z
      AYH=0.
      AZH=0.

C IF CHANGED, CONSIDER FOLLOWING LOOP AND SR MYBFELD }

      cdexpomx=cdexp(dcmplx(dnull,xkfour(1)*(-zin)))
      dcsxkx=dreal(cdexpomx)
      dsnxkx=dimag(cdexpomx)

      dexpomy=dexp(ykfour(1)*yin)
      expomy=1.0d0

      cdexpomz=cdexp(dcmplx(dnull,zkfour(1)*    x ))
      cexpomz=dcmplx(1.0d0,dnull)

      do k=1,nfour-1

        if (xk0four.ne.0.0d0) then
          expomy=dexp(ykfour(k)*yin)
        else
          expomy=expomy*dexpomy
        endif

        expomy1=1.0d0/expomy
        dchyky=(expomy+expomy1)*0.5d0
        dshyky=(expomy-expomy1)*0.5d0

        cexpomz=cexpomz*cdexpomz
        dcszkz=dreal(cexpomz)
        dsnzkz=dimag(cexpomz)

        bxh=bxh-a(k)*xkfour(k)/ykfour(k)*dsnxkx*dshyky*dcszkz
        byh=byh+a(k)*                    dcsxkx*dchyky*dcszkz
        bzh=bzh-a(k)*zkfour(k)/ykfour(k)*dcsxkx*dshyky*dsnzkz



        AXH=AXH+A(K)/ZKFOUR(K)*DCSXKX*DCHYKY*DSNZKZ
        AZH=AZH+0.0

        AYH=AYH+A(K)/ZKFOUR(K)*XKFOUR(K)/YKFOUR(K)*DSNXKX*DSHYKY*DSNZKZ

      enddo

      bzout=-bxh
      byout= byh
      bxout= bzh

      azout=-axh
      ayout= ayh
      axout= azh

      return
      end
