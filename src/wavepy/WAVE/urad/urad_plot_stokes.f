*CMZ :  4.00/13 16/11/2021  21.30.23  by  Michael Scheer
*CMZ :  3.05/05 09/07/2018  13.41.08  by  Michael Scheer
*CMZ :  3.05/04 05/07/2018  12.40.34  by  Michael Scheer
*CMZ :  3.03/04 01/12/2017  14.34.43  by  Michael Scheer
*CMZ :  3.02/04 13/03/2015  10.30.49  by  Michael Scheer
*CMZ :  2.70/12 01/03/2013  15.40.59  by  Michael Scheer
*CMZ :  2.68/05 07/09/2012  11.23.42  by  Michael Scheer
*CMZ :  2.68/04 03/09/2012  11.49.18  by  Michael Scheer
*CMZ :  2.68/03 31/08/2012  09.22.41  by  Michael Scheer
*-- Author : Michael Scheer
*KEEP,gplhint.
!******************************************************************************
!
!      Copyright 2013 Helmholtz-Zentrum Berlin (HZB)
!      Hahn-Meitner-Platz 1
!      D-14109 Berlin
!      Germany
!
!      Author Michael Scheer, Michael.Scheer@Helmholtz-Berlin.de
!
! -----------------------------------------------------------------------
!
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy (wave_gpl.txt) of the GNU General Public
!    License along with this program.
!    If not, see <http://www.gnu.org/licenses/>.
!
!    Dieses Programm ist Freie Software: Sie koennen es unter den Bedingungen
!    der GNU General Public License, wie von der Free Software Foundation,
!    Version 3 der Lizenz oder (nach Ihrer Option) jeder spaeteren
!    veroeffentlichten Version, weiterverbreiten und/oder modifizieren.
!
!    Dieses Programm wird in der Hoffnung, dass es nuetzlich sein wird, aber
!    OHNE JEDE GEWAEHRLEISTUNG, bereitgestellt; sogar ohne die implizite
!    Gewaehrleistung der MARKTFAEHIGKEIT oder EIGNUNG FueR EINEN BESTIMMTEN ZWECK.
!    Siehe die GNU General Public License fuer weitere Details.
!
!    Sie sollten eine Kopie (wave_gpl.txt) der GNU General Public License
!    zusammen mit diesem Programm erhalten haben. Wenn nicht,
!    siehe <http://www.gnu.org/licenses/>.
!
!******************************************************************************
*KEND.
      subroutine urad_plot_stokes(nene,ener,stokes,xpl,ypl)

      implicit none

      integer iene,nene
      double precision ener(nene),stokes(4,nene)

      real xpl(nene),ypl(nene),xmin,xmax,ymin,ymax,s0,s1,s2,s3

      call mshplt_init(20,15.,15.,25,25,600,600,'urad_stokes.eps','','',0.)

      call mshplt_set_text_color(1,0,0,0)
      call mplopt('DATE',1)
      call mplset('GSIZ',0.35)
      call mtitle('Stokes Vectors')
      call mshplt_hplset('YGTI',-0.3)

      xmin=1.0e30
      xmax=-1.0e30
      ymin=1.0e30
      ymax=-1.0e30

      do iene=1,nene
        xpl(iene)=sngl(ener(iene))
        s0=sngl(stokes(1,iene))
        s1=sngl(stokes(2,iene))
        s2=sngl(stokes(3,iene))
        s3=sngl(stokes(4,iene))
        if (xpl(iene).lt.xmin) xmin=xpl(iene)
        if (xpl(iene).gt.xmax) xmax=xpl(iene)
        if (s0.lt.ymin) ymin=s0
        if (s0.gt.ymax) ymax=s0
        if (s1.lt.ymin) ymin=s1
        if (s1.gt.ymax) ymax=s1
        if (s2.lt.ymin) ymin=s2
        if (s2.gt.ymax) ymax=s2
        if (s3.lt.ymin) ymin=s3
        if (s3.gt.ymax) ymax=s3
        ypl(iene)=s0
      enddo

      call mshplt_frame(
     &  xmin-(xmax-xmin)*0.1,xmax+(xmax-xmin)*0.1,
     &  ymin-(ymax-ymin)*0.1,ymax+(ymax-ymin)*0.1,
     &  'photon energy [ev]','Nph/s/mm**2/0.1%BW/100mA','')

      call mshplt_set_line_color(2,0,0,0)
      call mshplt_pline(nene,xpl,ypl)
      call mshplt_set_text_color(2,0,0,0)
      call mshplt_text_ndc(0.9,0.95,'S0')

      do iene=1,nene
        xpl(iene)=sngl(ener(iene))
        ypl(iene)=sngl(stokes(2,iene))
      enddo
      call mshplt_set_text_color(3,0,0,0)
      call mshplt_set_line_color(3,0,0,0)
      call mshplt_pline(nene,xpl,ypl)
      call mshplt_text_ndc(0.9,0.92,'S1')

      do iene=1,nene
        xpl(iene)=sngl(ener(iene))
        ypl(iene)=sngl(stokes(3,iene))
      enddo
      call mshplt_set_text_color(4,0,0,0)
      call mshplt_set_line_color(4,0,0,0)
      call mshplt_pline(nene,xpl,ypl)
      call mshplt_text_ndc(0.9,0.89,'S2')

      do iene=1,nene
        xpl(iene)=sngl(ener(iene))
        ypl(iene)=sngl(stokes(4,iene))
      enddo
      call mshplt_set_text_color(5,0,0,0)
      call mshplt_set_line_color(5,0,0,0)
      call mshplt_pline(nene,xpl,ypl)
      call mshplt_text_ndc(0.9,0.86,'S3')
      call mshplt_set_text_color(1,0,0,0)

      call mshplt_end

      return
      end
