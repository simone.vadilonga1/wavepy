#!/bin/sh

# +PATCH,//WAVE/SHELL
# +DECK,wgfl_standalone_omp,T=SHELL.

OLDPWD=$PWD

cd $WAVE/for

rm -f wave*.o
rm -f $WAVE/bin/wave.exe

echo Compiling wave_mh_book.f

gfortran -c -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic \
-ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
-o $WAVE/for/wave_mh_book.o \
$WAVE/for/wave_mh_book.f

echo Compiling wave_mshplt.f

gfortran -c -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic \
-ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
-o $WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshplt.f

echo Compiling wave_mshcern.f

gfortran -c -w -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic \
-ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
-o $WAVE/for/wave_mshcern.o \
$WAVE/for/wave_mshcern.f


gfortran -c -O2 -cpp \
-fopenmp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-finit-local-zero \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/aaaaab.f

echo Compiling wave_f90_modules.f

gfortran -c -O2 -cpp \
-fopenmp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
$WAVE/for/wave_f90_modules.f

echo Compiling wave_user.f

gfortran -c -O2 \
-fno-automatic \
-fcheck=bounds \
-ffpe-summary=invalid,zero,overflow \
-finit-local-zero \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/wave_user.f


echo Compiling wave_omp.f

gfortran -c -O2 -cpp \
-fcheck=bounds \
-fopenmp \
-fbacktrace \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-finit-local-zero \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/wave_omp.f

echo Compiling wave_standalone.f

gfortran -c -O2 -cpp \
-finit-local-zero \
-fcheck=bounds \
-fbacktrace \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-fno-automatic \
-Wno-align-commons -ffixed-line-length-none -finit-local-zero -funroll-loops \
-o $WAVE/for/wave_standalone.o \
$WAVE/for/wave_standalone.f

echo Linking wave_standalone

gfortran -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fopenmp \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic \
-ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
-o $WAVE/bin/wave.exe \
$WAVE/for/wave_mh_book.o \
$WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshcern.o \
$WAVE/for/wave_omp.o \
$WAVE/for/wave_user.o \
$WAVE/for/wave_standalone.o

#$WAVE/for/wave_omp_modules.o \
cd $OLDPWD
