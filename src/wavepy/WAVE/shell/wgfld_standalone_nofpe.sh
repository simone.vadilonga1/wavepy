#!/bin/sh

# +PATCH,//WAVE/SHELL
# +DECK,wgfld_standalone_nofpe,T=SHELL.

OLDPWD=$PWD

cd $WAVE/for

echo compiling $WAVE/for/wave_mshplt.f

gfortran -c -g -cpp \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshplt.f

echo compiling $WAVE/for/wave_mshcern.f

gfortran -c -w -g -cpp \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_mshcern.o \
$WAVE/for/wave_mshcern.f

echo compiling $WAVE/for/wave_msh_hbook.f

# In case of segmentation faults, use -O0
gfortran -c -O0 -cpp \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_msh_hbook.o \
$WAVE/for/wave_msh_hbook.f

gfortran -c -g -cpp \
-fopenmp \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/aaaaab.f

echo compiling $WAVE/for/wave_f90_modules.f

gfortran -c -g -cpp \
-fopenmp \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/wave_f90_modules.f


echo compiling $WAVE/for/wave_omp.f

gfortran -c -g -cpp \
-fopenmp \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/wave_omp.f

echo compiling $WAVE/for/wave_standalone.f

gfortran -c -g -cpp \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_standalone.o \
$WAVE/for/wave_standalone.f

echo Linking $WAVE/bin/wave.exe

gfortran -g -cpp \
-fopenmp \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/bin/wave.exe \
$WAVE/for/wave_msh_hbook.o \
$WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshcern.o \
$WAVE/for/wave_omp.o \
$WAVE/for/wave_standalone.o

cd $OLDPWD
