#!/bin/sh

# +PATCH,//WAVE/SHELL
# +DECK,wgfl_standalone_static,T=SHELL.

OLDPWD=$PWD

cd $WAVE/for

echo compiling $WAVE/for/wave_mshplt.f

gfortran -c -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshplt.f

echo compiling $WAVE/for/wave_mshcern.f

gfortran -c -w -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_mshcern.o \
$WAVE/for/wave_mshcern.f

echo compiling $WAVE/for/wave_msh_hbook.f

# In case of segmentation faults, use -O0
gfortran -c -O0 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_msh_hbook.o \
$WAVE/for/wave_msh_hbook.f

gfortran -c -O2 -cpp \
-fopenmp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/aaaaab.f

echo compiling $WAVE/for/wave_f90_modules.f

gfortran -c -O2 -cpp \
-fopenmp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/wave_f90_modules.f


echo compiling $WAVE/for/wave_omp.f

gfortran -c -O2 -cpp \
-fopenmp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons \
-ffixed-line-length-none -funroll-loops \
$WAVE/for/wave_omp.f

echo compiling $WAVE/for/wave_standalone.f

gfortran -c -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/for/wave_standalone.o \
$WAVE/for/wave_standalone.f

echo Linking $WAVE/bin/wave.exe satically

gfortran -static -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fopenmp \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/bin/wave.exe \
$WAVE/for/wave_msh_hbook.o \
$WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshcern.o \
$WAVE/for/wave_omp.o \
$WAVE/for/wave_standalone.o

cd $OLDPWD
