#!/bin/sh

# +PATCH,//WAVE/SHELL
# +DECK,link_wave_standalone,T=SHELL.

OLDPWD=$PWD

cd $WAVE/for

echo Linking $WAVE/bin/wave.exe

gfortran -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fopenmp \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none -funroll-loops \
-o $WAVE/bin/wave.exe \
$WAVE/for/wave_msh_hbook.o \
$WAVE/for/wave_mshplt.o \
$WAVE/for/wave_mshcern.o \
$WAVE/for/wave_omp.o \
$WAVE/for/wave_standalone.o

cd $OLDPWD
