#!/bin/sh

# +PATCH,//WAVE/SHELL
# +DECK,mklib_urad_incl,T=SHELL.

OLDPWD=$PWD

cd $WAVE_INCL/urad/mod

echo
echo
echo Compiling "$WAVE_INCL/urad/mod/*.f"
echo

gfortran -c -w -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
*.f

echo
echo Making $WAVE_INCL/lib/liburad_modules.a
echo

ar rc $WAVE_INCL/lib/liburad_modules.a *.o
ranlib $WAVE_INCL/lib/liburad_modules.a

mv *.mod ..

cd $WAVE_INCL/urad

echo
echo
echo Compiling "$WAVE_INCL/urad/*.f"
echo

gfortran -c -w -O2 -cpp \
-ffpe-summary=invalid,zero,overflow \
-fdec -fd-lines-as-comments \
-Wno-align-commons -fno-automatic -ffixed-line-length-none \
-finit-local-zero \
-funroll-loops \
*.f

echo
echo Making $WAVE_INCL/lib/liburad.a
echo

ar rc $WAVE_INCL/lib/liburad.a *.o
ranlib $WAVE_INCL/lib/liburad.a

cd $OLDPWD
