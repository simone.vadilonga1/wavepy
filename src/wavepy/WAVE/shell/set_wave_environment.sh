# +PATCH,//WAVE/SHELL
# +DECK,set_wave_environment,T=SHELL.

if test -z $WAVE; then

  echo
  echo Environment variable WAVE is not defined. Please define it, e.g.:
  echo export WAVE=/home/user/wave
  echo

else

if test -d $WAVE; then

cd $WAVE

bash $WAVE/shell/title.sh $WAVE

alias es='$EDITOR $WAVE/shell/set_wave_environment.sh'
alias ss='. $WAVE/shell/set_wave_environment.sh'
alias cdwave='. $WAVE/shell/set_wave_environment.sh'

alias wplot='ipython3 -i $WAVE/python/waveplot.py'

alias wave='$WAVE/stage/wave'
alias waves='cd $WAVE/stage; bash $WAVE/shell/title.sh; ipython3 -i $WAVE/python/waves.py'

cd $WAVE/stage
bash $WAVE/shell/title.sh

else

  echo
  echo Error: $WAVE not found or it is not a directory!
  echo
fi
fi
